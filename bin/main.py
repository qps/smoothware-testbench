
import config

import bench_setup
import database_setup
import test_setup

from smoothwaretb.test_collection import TestCollection

import user_inputs

testbench = bench_setup.create_testbench()
database_handler = database_setup.create_database_handler()

channel_config = user_inputs.get_channel_setup(testbench.devices['uqds'])

database_handler.setup_channels(channel_config['batch'],
                                channel_config['channel_ids'],
                                channel_config['channel_types'])

test_selections = user_inputs.select_tests()

collection = TestCollection(database_adapter=database_handler)

test_setup.construct_test(testbench, collection, test_selections,
                          len(channel_config['channel_ids']))

collection.run()