
from smoothwaretb.database.uqds_db_handler import UqdsDBHandler
import database_credentials


def create_database_handler():
    return UqdsDBHandler(database_credentials.connection_details())