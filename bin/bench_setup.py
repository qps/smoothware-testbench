import config

from smoothwaretb.testbench import Testbench

from smoothwaretb.device_wrappers.sine_generator import SineDevice
from smoothwaretb.device_wrappers.matrix import MatrixDevice
from smoothwaretb.device_wrappers.amplifier import AmplifierDevice
from smoothwaretb.device_wrappers.uqds import UQDSDevice
from smoothwaretb.device_wrappers.dc_source import DCSourceDevice
from smoothwaretb.device_wrappers.dc_meter import DCMeterDevice

from devicelib.ds360 import DS360
from devicelib.keysight_33500 import Keysight33500
from devicelib.keysight_33502a import Keysight33502A
from devicelib.keysight_34972a import Keysight34972A
from devicelib.digistant_4462 import Digistant4462
from devicelib.keithley_2002 import Keithley2002
from devicelib.batterybox_v2 import BatteryBoxV2

from uqdslib.uqds import UQDS


def create_testbench():
    output_bench = Testbench()

    output_bench.add_device('stim', SineDevice(Keysight33500(name=config.KEYSIGHT_ID), amplification=2.5))

    output_bench.add_device('thd_stim',
                            SineDevice(DS360(config.DS360_PORT), amplification=1, switch_wait_time=4))

    output_bench.add_device('battery',
                            DCSourceDevice(BatteryBoxV2(name=config.BATTERY_ID)))

    dc_source = DCSourceDevice(Digistant4462(port_name=config.DIGISTANT_ID))
    output_bench.add_device('dc', dc_source)

    output_bench.add_device('dcmeter',
                            DCMeterDevice(Keithley2002(name=config.KEYTHLEY_2002_ID)))

    uqds = UQDSDevice(UQDS(config.UQDS_PORT),
                      register_map_path=config.UQDS_REG_MAP,
                      config_block=config.UQDS_CONFIG_BLOCK)

    output_bench.add_device('matrix',
                            MatrixDevice(Keysight34972A(name=config.MATRIX_ID)))

    output_bench.add_device('stim_amp', AmplifierDevice(Keysight33502A(name=config.AMPLIFIER_ID)))

    output_bench.add_device('uqds', uqds)

    return output_bench
