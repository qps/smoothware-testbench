import numpy as np

from smoothwaretb.device_wrappers.uqds_sweep import UqdsTestSweep


def add_linearity_cal(testbench, collection, gain, channel_count):
    linearity_test = UqdsTestSweep(testbench,
                                   channel_count=channel_count,
                                   sample_count=8192,
                                   pga_setting=gain)

    linearity_test.sweep_type = 'linearity_cal'

    linearity_test.set('uqds.calibration_disabled', True)
    linearity_test.set('uqds.decimation_enabled', True)

    linearity_test.set('matrix.source', 2)
    linearity_test.set('matrix.hold_source', True)

    if gain >= 45:
        linearity_test.set('matrix.switch_wait_time', 180)

    linearity_test.set('dcmeter.enabled', True)

    voltage_range_selects = {
        1: 20,
        9: 20,
        45: 2,
        450: 0.2
    }
    linearity_test.set('dcmeter.voltage_range', voltage_range_selects[gain])

    lin_points = np.linspace(-19 / gain, 19 / gain, 8, dtype=float)
    lin_points_shuffle = np.array(lin_points)
    
    np.random.default_rng().shuffle(lin_points_shuffle)

    # for i in range(len(lin_points)):
    #     if i % 2 == 0:
    #         lin_points_shuffle.append(lin_points[int(np.floor(len(lin_points) / 2) + np.floor(i / 2.0))])
    #     else:
    #         lin_points_shuffle.append(lin_points[int(np.floor(len(lin_points) / 2) - np.ceil(i / 2.0))])

    linearity_test.uqds_linearity_sweep('dc.voltage', 'dcmeter.voltage',
                                        lin_points_shuffle,
                                        save_calibration=True)

    linearity_test.evaluate_data(calculate_linearity_along='dcmeter.voltage',
                                 statistics={
                                     'stddev': {
                                         'max': 250e-6,
                                         'avg_max': 200e-6
                                     },
                                     'avg_to_stimulus_difference': {
                                         'max': (30e-3 / gain) + 200e-6,
                                         'min': (-30e-3 / gain) - 200e-6
                                     }
                                 })

    collection.add_sweep(linearity_test)


def add_linearity(testbench, collection, gain, channel_count):
    linearity_test = UqdsTestSweep(testbench,
                                   channel_count=channel_count,
                                   sample_count=8192,
                                   pga_setting=gain)

    linearity_test.sweep_type = 'linearity'

    linearity_test.set('matrix.source', 2)
    linearity_test.set('matrix.switch_wait_time', 20)

    linearity_test.set('dcmeter.enabled', True)
    linearity_test.set('uqds.decimation_enabled', True)

    voltage_range_selects = {
        1: 20,
        9: 20,
        45: 2,
        450: 0.2
    }
    linearity_test.set('dcmeter.voltage_range', voltage_range_selects[gain])

    lin_points = np.linspace(-19 / gain, 19 / gain, num=18 if gain == 1 else 14, dtype=float)
    lin_points_shuffle = np.array(lin_points)

    np.random.default_rng().shuffle(lin_points_shuffle)

    # for i in range(len(lin_points)):
    #     if i % 2 == 0:
    #         lin_points_shuffle.append(lin_points[int(np.floor(len(lin_points) / 2) + np.floor(i / 2.0))])
    #     else:
    #         lin_points_shuffle.append(lin_points[int(np.floor(len(lin_points) / 2) - np.ceil(i / 2.0))])

    linearity_test.uqds_linearity_sweep('dc.voltage', 'dcmeter.voltage',
                                        lin_points_shuffle,
                                        save_calibration=False)

    linearity_test.evaluate_data(calculate_linearity_along='dcmeter.voltage',
                                 statistics={
                                     'stddev': {
                                         'max': 250e-6,
                                         'avg_max': 200e-6
                                     },
                                     'avg_to_stimulus_difference': {
                                         'max': 200e-6,
                                         'min': -200e-6,
                                         'avg_max': 30e-6,
                                         'avg_min': -30e-6
                                     }
                                 })

    collection.add_sweep(linearity_test)


def add_noise(testbench, collection, gain, channel_count):
    noise_test = UqdsTestSweep(testbench,
                               channel_count=channel_count,
                               sample_count=4096,
                               pga_setting=gain)

    noise_test.sweep_type = 'noise'

    noise_test.set('matrix.source', 1)

    possible_values = [-21, -19.5, -18, -16.5, -15, -13.5, -12, -9, -7.5, -6, -4.5, -3,
                       -1.5, 1.5, 3, 4.5, 6, 7.5, 9, 12, 13.5, 15, 16.5, 18, 19.5, 21]
    selected_values = []
    for voltage in possible_values:
        if abs(voltage) < 20 / gain:
            selected_values.append(voltage)

    noise_test.sweep('battery.voltage', selected_values)

    noise_test.evaluate_data(
        save_histogram=True,
        statistics={
            'stddev': {
                'max': 250e-6,
                'avg_max': 150e-6
            }
        })

    collection.add_sweep(noise_test)


def add_thd(testbench, collection, gain, channel_count):
    thd_sweep = UqdsTestSweep(testbench,
                              channel_count=channel_count,
                              sample_count=8192,
                              pga_setting=gain)

    thd_sweep.sweep_type = "thd"

    thd_sweep.set('matrix.source', 4)
    thd_sweep.set('stim_amp.channel', 1)
    thd_sweep.set('stim_amp.amp_enabled', False)

    thd_sweep.uqds_fft_sweep('thd_stim.frequency', 'thd_stim.amplitude', 200, 50000, steps=20,
                             amplitude=10 / gain)

    maxima = {
        1: -60,
        9: -60,
        45: -55,
        450: -50
    }

    thd_sweep.evaluate_data(statistics={
        'thd_db': {
            'max': maxima[gain]
        }
    })

    collection.add_sweep(thd_sweep)


def add_bandwidth(testbench, collection, gain, channel_count):
    bandwidth_sweep = UqdsTestSweep(testbench,
                                    channel_count=channel_count,
                                    sample_count=8192,
                                    pga_setting=gain)

    bandwidth_sweep.sweep_type = "bandwidth"

    bandwidth_sweep.set('matrix.source', 4)
    bandwidth_sweep.set('stim_amp.channel', 2)
    bandwidth_sweep.uqds_fft_sweep('stim.frequency', 'stim.amplitude', 200, 300000, steps=30,
                                   amplitude=10 / gain)

    avg_maxima = {
        1: 2,
        9: 3,
        45: 4,
        450: 14
    }

    avg_minima = {
        1: 1,
        9: 1.5,
        45: 3,
        450: 9
    }

    bandwidth_sweep.evaluate_data(calculate_3db_point=True, statistics={
        'stimulus_attenuation_db': {
            'max': 40 if gain <= 45 else 60,
            'min': -0.1,
            'avg_max': avg_maxima[gain],
            'avg_min': avg_minima[gain]
        }
    })

    collection.add_sweep(bandwidth_sweep)


def add_cmrr(testbench, collection, gain, channel_count):
    cmrr_sweep = UqdsTestSweep(testbench,
                               channel_count=channel_count,
                               sample_count=8192,
                               pga_setting=gain)

    cmrr_sweep.sweep_type = "cmrr"

    cmrr_sweep.set('matrix.source', 3)
    cmrr_sweep.set('stim_amp.channel', 2)

    cmrr_sweep.uqds_fft_sweep('stim.frequency', 'stim.amplitude', 500, 300000, steps=25)

    cmrr_sweep.evaluate_data(statistics={
        'stimulus_attenuation_db': {
            'min': 80,
            'max': 190
        }
    })

    collection.add_sweep(cmrr_sweep)


def construct_test(testbench, collection, test_selection, channel_count):
    for gain in test_selection['gains']:
        if 'linearity_cal' in test_selection['tests']:
            add_linearity_cal(testbench, collection, gain, channel_count)

        if 'linearity' in test_selection['tests']:
            add_linearity(testbench, collection, gain, channel_count)

        if (gain <= 9) and ('noise' in test_selection['tests']):
            add_noise(testbench, collection, gain, channel_count)

        if 'bandwidth' in test_selection['tests']:
            add_bandwidth(testbench, collection, gain, channel_count)

        if 'cmrr' in test_selection['tests']:
            add_cmrr(testbench, collection, gain, channel_count)

        if 'thd' in test_selection['tests']:
            add_thd(testbench, collection, gain, channel_count)
