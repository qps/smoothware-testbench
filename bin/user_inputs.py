import config


def get_channel_setup(uqds):
    batch = input(f"Please enter batch ID of these channels [{config.DEFAULT_BATCH}]: ")
    if len(batch) == 0:
        batch = config.DEFAULT_BATCH

    #channel_type = input(f"Please enter the type of the channels [{config.DEFAULT_CHANNEL_TYPE}]: ")
    #if len(channel_type) == 0:
    #    channel_type = config.DEFAULT_CHANNEL_TYPE

    channel_type = config.DEFAULT_CHANNEL_TYPE

    print("Please enter the IDs of the channels (last 4 digits, confirm with an empty ID):")
    channel_ids = []
    channel_types = []
    for i in range(16):
        next_id = input(f" - Channel ID of slot {i}: ")

        if len(next_id) == 0:
            break

        channel_ids.append(int(next_id))
        channel_types.append(channel_type)

    if input('Is this info correct? [y/N]:').lower() != 'y':
        print('Exiting script...')
        exit()

    print(f"Testing {len(channel_ids)} channels!")

    for i in range(len(channel_ids)):
        channel_ids[i] = uqds.program_channel_id(i, channel_ids[i])

    return {
        'batch': batch,
        'channel_ids': channel_ids,
        'channel_types': channel_types
    }


def select_tests():
    gain_list = input('Which gains to test? [1, 9, 45, 450]:')
    if len(gain_list) == 0:
        gain_list = '1, 9, 45, 450'
    gain_list = gain_list.split(',')

    gain_list = map(lambda x: int(x), gain_list)

    print('Available tests are: linearity_cal, linearity, noise, bandwidth, cmrr, thd')
    test_list = input('Which tests to perform? [all]:')
    if (len(test_list) == 0) or test_list == 'all':
        test_list = 'linearity_cal, linearity, noise, bandwidth, cmrr, thd'
    test_list = test_list.split(',')

    test_selection = {}
    for test in test_list:
        test_selection[test.strip()] = True

    if input('Is this info correct? [y/N]:').lower() != 'y':
        print('Exiting script...')
        exit()

    return {
        'gains': gain_list,
        'tests': test_selection
    }
