
from context import smoothwaretb as stb

import numpy as np 
import matplotlib.pyplot as plt

stimulus_freq = 376

test_signal = 10 * np.sin(np.arange(32768, dtype=np.float64) / 32768.0 * np.pi * 2 * stimulus_freq * 1)

test_signal_noisy = test_signal + np.random.normal(0, 0.01, size=32768) + 0.02 * np.mod((np.arange(32768) / 32768.0 * stimulus_freq), 1)

clean_wrapper = stb.BufferWrapper(test_signal, 1/32768.0)
noisy_wrapper = stb.BufferWrapper(test_signal_noisy, 1/32768.0)

clean_wrapper.set_stimulus(stimulus_freq, 10)
noisy_wrapper.set_stimulus(stimulus_freq, 10)

print(f"Noise Floor is: {clean_wrapper.noise_floor_db} vs. {noisy_wrapper.noise_floor_db}")

print(f"Amplitude at stimulus freq.: {clean_wrapper.get_fft_peak(stimulus_freq)} vs {noisy_wrapper.get_fft_peak(stimulus_freq)}")

print(f"THD is: {clean_wrapper.thd_db} vs. {noisy_wrapper.thd_db}")
print(f"Harmonics are: {noisy_wrapper.harmonic_amplitudes_db}")

print(f"Attenuation is: {clean_wrapper.stimulus_attenuation_db} vs. {clean_wrapper.stimulus_attenuation_db}")

print(f"SNR is: {clean_wrapper.snr_db} vs. {noisy_wrapper.snr_db}")

plt.loglog(noisy_wrapper.get_fft())
plt.show()