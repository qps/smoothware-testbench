
from context import smoothwaretb as stb

mock_dev = stb.testdevice.MockTestDevice({'test':'10'})

testbench = stb.Testbench()
testbench.add_device('Test 1', mock_dev)

test_run = stb.testexec.TestSweep(testbench);

test_run.sweep('Test 1.amplitude', range(10));
test_run.sweep('Test 1.frequency', range(10));

test_run.run()
