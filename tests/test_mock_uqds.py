
import psycopg2


from context import smoothwaretb as stb
import smoothwaretb.device_wrappers.uqds
from smoothwaretb.device_wrappers.uqds_sweep import UqdsTestSweep

from smoothwaretb.database.uqds_db_handler import UqdsDBHandler

import numpy as np 
import matplotlib.pyplot as plt

import database_setup

#database_interface = UqdsDBHandler(database_setup.new_connection())

#database_interface._DROP_SCHEMA()
#database_interface.setup_schema()

#database_interface.setup_channels('TESTBATCH', ['TEST1', 'TEST2'], ['TEST', 'TEST'])

testbench = stb.testbench.Testbench()

mock_uqds = smoothwaretb.device_wrappers.uqds.MockUQDSDevice()
testbench.add_device('uqds', mock_uqds)

collection = stb.test_collection.TestCollection(None) #database_interface)

test_run = UqdsTestSweep(testbench, channel_count = 2)
test_run.sweep_type = 'bandwidth'
test_run.uqds_fft_sweep('uqds.mock_stim_freq', 'uqds.mock_stim_amplitude', 1000, 100000, steps=150)

collection.add_sweep(test_run)

test_run = UqdsTestSweep(testbench, channel_count = 2, sample_count = 4096)
test_run.sweep_type = 'cmrr'
test_run.uqds_fft_sweep('uqds.mock_stim_freq', 'uqds.mock_stim_amplitude', 10000, 300000, steps=50)

collection.add_sweep(test_run)

collection.run()