UQDS_PORT = 'COM18'
REG_MAP_PATH = r"C:\UQDS\uQDS_2x_firmware\DOC\uQDS2/"

SWITCH_ID = 'USB0::0x0957::0x2007::MY59001715::INSTR'
BATTERY_ID = 'Dev1'
DIGISTANT_ID = 'COM13'
KEYTHLEY_2002_ID = 'GPIB0::16::INSTR'

from smoothwaretb.database.uqds_db_handler import UqdsDBHandler
import time

import database_setup

database_interface = UqdsDBHandler(database_setup.connection_details())

from context import smoothwaretb as stb

import smoothwaretb.device_wrappers.sine_generator
import smoothwaretb.device_wrappers.matrix
import smoothwaretb.device_wrappers.uqds
import smoothwaretb.device_wrappers.uqds_sweep
import smoothwaretb.device_wrappers.dc_source
import smoothwaretb.device_wrappers.dc_meter

from devicelib.keysight_33500 import Keysight33500
from devicelib.keysight_34972a import Keysight34972A
from devicelib.digistant_4462 import Digistant4462
from devicelib.keithley_2002 import Keithley2002
from devicelib.batterybox_v2 import BatteryBoxV2

from uqdslib.uqds import UQDS

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('tkagg')

stimulus_wrapper = smoothwaretb.device_wrappers.sine_generator.SineDevice(
    Keysight33500(name="USB0::0x0957::0x2807::MY57400962::INSTR"))

matrix = smoothwaretb.device_wrappers.matrix.MatrixDevice(Keysight34972A(name=SWITCH_ID))
battery = smoothwaretb.device_wrappers.dc_source.DCSourceDevice(BatteryBoxV2(name=BATTERY_ID))
digistant = smoothwaretb.device_wrappers.dc_source.DCSourceDevice(Digistant4462(port_name=DIGISTANT_ID))
uqds = smoothwaretb.device_wrappers.uqds.UQDSDevice(UQDS(UQDS_PORT), register_map_path=REG_MAP_PATH, config_block=18)
dcmeter = smoothwaretb.device_wrappers.dc_meter.DCMeterDevice(Keithley2002(name=KEYTHLEY_2002_ID))

testbench = stb.testbench.Testbench()
testbench.add_device("stim", stimulus_wrapper)
testbench.add_device("battery", battery)
testbench.add_device("dc", digistant)

testbench.add_device('dcmeter', dcmeter)
testbench.add_device("uqds", uqds)

testbench.add_device("matrix", matrix)

channel_names = []
channel_types = []

for j in range(16):
    channel_names.append(f'TEST_{j}')
    channel_types.append('TEST')

database_interface.setup_channels(f'TESTBATCH', channel_names, channel_types)
collection = stb.test_collection.TestCollection(database_adapter=database_interface)


def add_test_suite(collection, pga_gain):
    bandwidth_sweep = smoothwaretb.device_wrappers.uqds_sweep.UqdsTestSweep(testbench,
                                                                            channel_count=16, sample_count=16384,
                                                                            pga_setting=pga_gain)
    bandwidth_sweep.sweep_type = "bandwidth"

    bandwidth_sweep.set('matrix.source', 4)
    bandwidth_sweep.uqds_fft_sweep('stim.frequency', 'stim.amplitude', 200, 300000, steps=100,
                                   amplitude=10 / pga_gain)

    bandwidth_sweep.evaluate_data(calculate_3db_point=True, statistics={
        'stimulus_attenuation_db': {
            'max': 40,
            'min': -1
        }
    })

    cmrr_sweep = smoothwaretb.device_wrappers.uqds_sweep.UqdsTestSweep(testbench,
                                                                       channel_count=16, sample_count=16384,
                                                                       pga_setting=pga_gain)
    cmrr_sweep.sweep_type = "cmrr"

    cmrr_sweep.set('matrix.source', 3)
    cmrr_sweep.set('stim.channel', 2)

    cmrr_sweep.uqds_fft_sweep('stim.frequency', 'stim.amplitude', 10000, 300000, steps=100)

    cmrr_sweep.evaluate_data(statistics={
        'stimulus_attenuation_db': {
            'max': 140,
            'min': 80
        }
    })

    noise_test = smoothwaretb.device_wrappers.uqds_sweep.UqdsTestSweep(testbench,
                                                                       channel_count=16, sample_count=16384,
                                                                       pga_setting=pga_gain)
    noise_test.sweep_type = 'noise'

    noise_test.set('matrix.source', 1)
    noise_test.sweep('battery.voltage', [-12, -9, -7.5, -6, -4.5, -3, -1.5, 1.5, 3, 4.5, 6, 7.5, 9, 12])

    noise_test.evaluate_data(statistics={
        'stddev': {
            'max': 250e-6,
            'avg_max': 150e-6
        }
    })

    linearity_test = smoothwaretb.device_wrappers.uqds_sweep.UqdsTestSweep(testbench, channel_count=16,
                                                                           sample_count=32768,
                                                                           pga_setting=pga_gain)
    linearity_test.sweep_type = 'linearity_cal'

    linearity_test.set('uqds.calibration_disabled', True)
    linearity_test.set('matrix.source', 2)
    linearity_test.set('dcmeter.enabled', True)

    linearity_test.uqds_linearity_sweep('dc.voltage', 'dcmeter.voltage', np.arange(-20 / pga_gain, 21 / pga_gain,
                                                                                   5 / pga_gain, dtype=float),
                                        save_calibration=True)

    linearity_test.evaluate_data(calculate_linearity_along='dcmeter.voltage',
                                 statistics={
                                     'stddev': {
                                         'max': 250e-6,
                                         'avg_max': 150e-6
                                     }
                                 })

    collection.add_sweep(linearity_test)

    linearity_test = smoothwaretb.device_wrappers.uqds_sweep.UqdsTestSweep(testbench, channel_count=16,
                                                                           sample_count=32768,
                                                                           pga_setting=pga_gain)
    linearity_test.sweep_type = 'linearity'

    linearity_test.set('matrix.source', 2)
    linearity_test.set('dcmeter.enabled', True)

    linearity_test.uqds_linearity_sweep('dc.voltage', 'dcmeter.voltage', np.arange(-20 / pga_gain, 21 / pga_gain,
                                                                                   2.5 / pga_gain, dtype=float),
                                        save_calibration=False)

    collection.add_sweep(linearity_test)

    if pga_gain == 1:
        collection.add_sweep(noise_test)

    collection.add_sweep(bandwidth_sweep)
    collection.add_sweep(cmrr_sweep)


add_test_suite(collection, 1)
add_test_suite(collection, 9)
add_test_suite(collection, 45)
add_test_suite(collection, 450)

collection.run()
