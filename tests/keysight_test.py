
from context import smoothwaretb as stb

import smoothwaretb.device_wrappers.sine_generator

from devicelib.keysight_33500 import Keysight33500


import numpy as np
import time

fgen = Keysight33500(name="USB0::0x0957::0x2807::MY57400962::INSTR")
fgen_wrapper = smoothwaretb.device_wrappers.sine_generator.SineDevice(fgen)

mock_acquisition = stb.testdevice.MockTestDevice()

testbench = stb.testbench.Testbench()
testbench.add_device("cm", fgen_wrapper)
testbench.add_device("uqds", mock_acquisition)

test_run = stb.TestSweep(testbench)

test_run.sweep("cm.amplitude", [2, 10])
test_run.sweep("cm.offset", np.arange(-2, 2, 0.5))
test_run.sweep("cm.frequency", np.logspace(np.log10(1), np.log10(300000), 5))

test_run.run()
