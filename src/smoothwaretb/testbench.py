import time
import re


def _reformat_device_settings_dict(device_dict):
    """Settings-Dictionary reformatter function
    
    Details:
        This function will take in a generic, single or two-layer
        dict representing the settings to be applied to the devices of
        the testbench, and formats it into a two-layer dict structured
        with the first layer's key being the device name,
        the second layer key being the device setting to change.

    Parameters:
    device_dict : dict
        The dict of settings to reformat. Can be in a mixed shape of 
        { "device.setting" : value } and { "device": {"setting": value}}
    
    Returns: dict
        A dict formatted into the {"device" : {"setting": value }} format
    """

    out_dict = {}

    for key in device_dict:
        extracted_key = re.search(r"^([^\.]+)\.(.*)$", key)
        if extracted_key is None:
            out_dict[key] = device_dict[key]
        else:
            device_name = extracted_key.group(1)
            out_dict.setdefault(device_name, {})
            out_dict[device_name][extracted_key.group(2)] = device_dict[key]

    return out_dict


class Testbench:
    """A class representing a single hardware testbench for test execution.

    Details:
        This class is meant to act as an abstract interface between the test code
        that runs various testing sweeps, and the hardware of the testbench itself.
        It provides a clean interface to set arbitrary hardware options in a fairly
        hardware-independent manner, and to run through data aquisition cycles.

        To do this, a Testbench holds a list of testdevice.Device instances.
        Each device is given a unique named key, through which it can be accessed.

        The test code can then perform actions such as resetting the entire test bench,
        which will reset each device, prepare device settings for acquisitions, update
        device settings, and perform acquisitions.

        The Testbench also provides a unified data output format for later code to properly
        process data.

    Example:
        bench = Testbench()
        bench.add_device('sine_wave', sine_generator_device)

        bench.reset()

        bench.activate({
            'sine_wave.amplitude' : 10,
            'sine_wave.offset' : 0,
            'sine_wave.frequency' : 1000
        })

        bench.set_output('sine_wave.frequency', 2000)

        data = bench.run_acquisition()

    """

    def __init__(self):
        ## dict of known devices. The key is the device name
        self.devices = {}

    def add_device(self, name, device):
        """Add a device to the devices dict.
        """

        self.devices[name] = device

    def reset(self):
        """
        Reset all devices of the testbench.

    @see smoothwaretb.testdevice.Device.reset()
        """

        for device in reversed(self.devices):
            self.devices[device].reset()

    def activate(self, init_settings=None):
        """Activate a testbench with known settings.

        Details:
            This function will do two things: It will *first* prepare
            all device output settings via testdevice.Device.prepare_outputs(), using
            the init_settings dict. It will *then* call Device.activate() on 
            each known device.

            This is in order to give certain devices, such as DC Outputs, 
            waveform generators etc. a known-good starting configuration.
            
            It is recommended but not required to use the init_settings {}
        
        Parameters:
            init_settings: dict of parameters to write to outputs on activation. May
                either have the form {'device.setting': value} or {'device': {'setting': value}},
                equivalent to set_outputs()

        Raises:
            ValueError
            raised if a key does not exist or is a invalid
            device key format.

        @see set_outputs()
        """

        if init_settings is None:
            init_settings = {}

        init_settings = _reformat_device_settings_dict(init_settings)

        for device in init_settings:
            self.devices[device].prepare_outputs(init_settings[device])

        for device in self.devices:
            self.devices[device].activate()

    def assert_output_exists(self, key):
        """Checks whether or not a device output option exists.

        Raises:
            ValueError
            raised if the provided key does not exist or is a invalid
            device key format.
        """

        extracted_key = re.search(r"^([^\.]+)\.(.*)$", key)
        if extracted_key is None:
            raise ValueError("Key must follow format <device>.<output_option>!")

        device_name = extracted_key.group(1)
        device_key = extracted_key.group(2)

        if device_name not in self.devices:
            raise ValueError(f"Device {device_name} is not known to the testbench!")
        if not self.devices[device_name].has_output(device_key):
            raise ValueError(f"Device {device_name} does not have output option {device_key}!")

    def set_output(self, key, value):
        """Set a single device option. 
        
        Details:
            Key must be in the format 'device.setting', 
            value types depend on the device and may be any
            valid python type. This function essentially wraps set_outputs().

        Raises:
            ValueError
            raised if the key does not exist or is a invalid
            device key format.

        @see set_outputs
        """

        set_dict = {}
        set_dict[key] = value
        self.set_outputs(set_dict)

    def set_outputs(self, data_dict):
        """Set a list of options
        
        Details:
            Will set multiple device options based on the provided dict. Dict can be a 
            mixed format of {"device.setting": value} and {"device": {"setting": value}}

            The data type of Value that is accepted depends on the Device, and 
            may be any valid python type.
        
        Raises:
            ValueError
            raised if a key does not exist or is a invalid
            device key format.
        """

        data_dict = _reformat_device_settings_dict(data_dict)

        for key in data_dict:
            self.devices[key].update_outputs(data_dict[key])

    def get_output_values(self):
        """Get the current output status of all devices

        Details:
            This function will grab each device's outputs via 
            testdevice.Device.get_outputs().
            It will then return a dict with keys being the device names, values
            being the return values of the testdevice.Device.get_outputs() call.

        Returns:
            dict with device names as keys, dicts of device output settings as values
        """

        out = {}
        for key in self.devices:
            output_data = self.devices[key].get_outputs()
            if output_data is not None:
                out[key] = output_data

        return out

    def get_output_values_flattened(self):
        """Alternate version of get_output_values returning outputs with 'device.setting' as keys

        Details:
            This function does the same as get_output_values(), but flattens them to a 
            single-layer dict before passing it further along.
            Useful for certain printing or database operations.

        Returns:
            dict
            Single-layer dict of the output of all testdevice.Device.get_outputs() 
            with keys being 'device.setting', values being values.
        """

        current_outputs = self.get_output_values()

        out_dict = {}

        for key in current_outputs:
            value = current_outputs[key]
            if isinstance(value, dict):
                for output_key in value:
                    out_dict[key + '.' + output_key] = value[output_key]
            elif value is None:
                continue
            else:
                out_dict[key] = value

        return out_dict

    def _print_acquisition_state(self, prefix, state):
        print(f"{prefix}: {state}", end=' ')

    def _run_acquisition_uncaught(self, status_line_prefix):
        results_dict = self.get_output_values_flattened()

        timing_dict = {}
        timing_dict['start'] = time.time()

        stages = ['prepare', 'trigger', 'wait']

        for stage in stages:
            self._print_acquisition_state(status_line_prefix, stage)
            for device in self.devices:
                self.devices[device].acquisition_step(stage)
            timing_dict[stage] = time.time()

        self._print_acquisition_state(status_line_prefix, 'downloading')

        results_dict['measurements'] = {}
        device_results = results_dict['measurements']

        for device in self.devices:
            data = self.devices[device].acquisition_step('download')
            if data is not None:
                device_results[device] = data

        self._print_acquisition_state(status_line_prefix, 'done!')

        return results_dict, timing_dict

    def run_acquisition(self, status_line_prefix="\33[2K\rRunning acquisition"):
        """Run an acquisition cycle.

        Details:
            This function will run a full acquisition cycle of the testbench.
            This is done in discrete steps. Each device's Device.acquisition_step()
            is called in the following stages:

            * prepare
                - Shall be used to set up non-timing-critical settings of the device
                for acquisition
            * trigger
                - *Timing critical*. MUST be completed quickly, to allow all devices 
                to be triggered at about the same time
            * wait
                - Wait for all devices to finish whatever work they do to acquire data
            * download
                - Download the data. This stage *may* return arbitrarily formatted data
                to be saved as measurement.
        
            Note that *all* errors are caught, and the acquisition is re-tried up to five 
            times. This is to allow for data communication errors not crashing the rest
            of the code.
            After the fifth re-try the error is re-raised.

        Parameters:
            status_line_prefix : str
                Optional string to be used for status line printing. Mainly used internally.

        Returns:
            [dict, dict]
            Two dicts. One containing measurement results, the other timing information 
            (which may be ignored).
            Measurement results are in the following format:
            - All device output settings are collected via get_output_values_flattened(), e.g. 
                { "sine_generator.frequency": 2000, "sine_generator.amplitude" : 10 }
            - The result of each download step, e.g. the non-output data that was acquired
              during this stage, is saved under the "measurements" key, followed by the device
              key. Note that this data may be free-form. E.g.:
                { "measurements": { "uqds": SomeDataType() }}
        """

        try_count = 0
        while True:
            try:
                try_count += 1

                return self._run_acquisition_uncaught(status_line_prefix)
            # pylint: disable=broad-exception-caught
            except Exception as exception:
                print(f'\nError in acquisition (try {try_count})!')
                print(exception, end='\n\n')

                if try_count > 5:
                    print("Retry count reached, aborting this acquisition...")

                    raise
