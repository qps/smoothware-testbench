
class TestCollection:
    """Test Sweep Collection class

    Details:
        This class deals with handling multiple consecutively executed
        test sweeps. It allows the user to specify a list of tests that may
        be executed, and will then execute them one by one.

        Its main purpose is to help automate the control of longer, more 
        complex test execution, by providing a way to set common parameters
        for multiple tests, such as the data event handling code.

        It also provides a logical grouping of tests together, in a way that
        is necessary for the database schema to function properly, by giving
        a parameter to identify groups of tests with.

        Tests are added to the collection via the TestCollection.add_sweep()
        function. Tests will be executed in the other they were added once 
        run() is called.

        Adding a test will automatically set the tests's event_handler parameter,
        to capture events and data to be sent to the database. No further
        user action is needed for this. Please note that when using a 
        database, it is highly advised to properly set the sweep_type parameter
        of the TestSweep!

    Example:
        collection = TestCollection(database_handler)
        
        sweep = TestSweep(bench)
        
        # Perform sweep test setup, e.g.
        sweep.sweep('sine_generator.frequency', [....])

        sweep.sweep_type = 'bandwidth'

        collection.add_sweep(sweep)

        collection.run()
    """


    def __init__(self, database_adapter = None):

        ## Database adapter instance. 
        # Must support calls to handle_testbench_event!
        # May be None.
        self.db_adapter = database_adapter

        ## Array of test_sweep.TestSweep instances to be executed
        # during a run
        self.sweeps_list = []

        ## Used by database code to attach an ID parameter for
        # database operations
        self.testbench_collection_id = None

    def _emit_event(self, data):
        data['test_collection'] = self

        if self.db_adapter is not None:
            self.db_adapter.handle_testbench_event(data)

    def add_sweep(self, sweep):
        """Add a sweep and connect it to the event chain.

        Details:
            This function will add the given sweep to the list of sweeps
            to be executed during a run() and will set it up to be 
            connected to the db_adapter, if one was given to this class.
        
        Note:
            It will also set up the sweep's test_sweep.TestSweep.event_handler,
            which, for proper functionaliy, must not be altered or set by the 
            user later on!

        Returns:
            test_sweep.TestSweep : The sweep that was initially given
        """

        # pylint: disable=unnecessary-lambda

        sweep.event_handler = lambda data: self._emit_event(data)

        self.sweeps_list.append(sweep)

        return sweep

    def get_total_point_count(self):
        """Return the total number of aquisiton points for all sweeps.

        Returns:
            Interger : The sum of all acquisition points for all 
            currently known sweeps.
        """

        count = 0

        for sweep in self.sweeps_list:
            count += sweep.get_total_point_count()
        
        return count

    def get_completed_point_count(self):
        """Return the number of acquisition points that have already been completed.

        Returns:
            Integer : The sum of all acquisition points that have been completed.
        """
        
        count = 0

        for sweep in self.sweeps_list:
            count += sweep.get_completed_point_count()

        return count


    def run(self):
        """Run all registered tests.

        Details:
            This function will execute all tests, in the order they
            were provided to via add_sweep().

        Returns:
            None

        Note:
            If inspecting the results of the sweeps later on is
            of interest, it is recommended to save references to the 
            sweeps added in add_sweep() and then act on those sweeps.
        """

        print("Starting testbench!")
        print(f"Testbenches loaded: {len(self.sweeps_list)}")
        print("The following measurements will be performed:")
        for sweep in self.sweeps_list:
            print(f" - {sweep.sweep_type} ({sweep.get_total_point_count()} acquisitions)")
        print(f"Total acquisition point count for all testbenches: {self.get_total_point_count()}")

        print("\n\n")

        self._emit_event({'type': 'test_collection_start'})

        for sweep in self.sweeps_list:
            self._emit_event({
                'type': 'test_sweep_setup',
                'test_sweep': sweep
            })

        for sweep in self.sweeps_list:
            print(f"\nBeginning test sweep: {sweep.sweep_type}")

            sweep.run()

            print("\nSweep complete!\n\n")

        self._emit_event({
            'type': 'test_collection_complete'
        })

        print("Full test suite comleted successfully!")
