import time

import numpy as np
import pandas as pd


class TestSweep:
    """Sweep handler class.

    Details:
        For test execution, it is most often required to acquire data at multiple
        points along some vector(s), such as signal DC component or frequency component.

        This class is meant to handle one such sweep event, and provide the needed 
        functionality to run through the data aquisition of it.

        In its most basic form, it merely needs to be passed a Testbench instance, and
        configured to one sweep.
        The data may then be processed and evaluated.

        This class also allows for the handling of live data events via a callback 
        function, the data_eval_fn. This function is used to implement database saving 
        and live status updating, and will mostly be used by the database handler classes, 
        but may be used for other purposes.
        Details of this event structure are provided in the _emit_event() function

        When the TestSweep.run() function is called, it will first reset the provided
        testbench, then prepare the devices and configure them to the setpoints given 
        via the TestSweep.set() function.

        It will then run through each vector of sweep data provided by 
        the TestSweep.sweep function, in the order the data was provided. Note that this is
        a cross product of sweep data, e.g. sweeping across six values for 'amplitude' and
        six values of 'frequency' will result in 36 total data acquisition points!

        Once the sweep is done, the run() function will return a Pandas data frame containing 
        the measurement data.
        The DataFrame columns are a join between a {} of the current sweep setpoints, and
        the data returned by the Testbench aquisition function. This means that each output
        will have a column of the name 'device.setting', and there will be one column named 
        'measurements' that contains a {} with duplicates of the measurement outputs 
        ('device.setting' as keys) and device names as keys and the acquisition 
        data as values.


    Example:

        sweep = TestSweep(bench)

        sweep.set('device.setting', True)
        sweep.set('sine_generator.amplitude', 10)
        
        sweep.sweep('sine_generator.frequency', [0, 1, 2, 3]) # Lists are allowed!

        sweep.sweep('sine_generator.offset', np.linspace(-19 / gain, 19 / gain,
                                                8, dtype=float)) # As well as numpy arrays

        # Note that there is a convenience function to perform frequency sweeps that fall
        # into FFT bins:
        sweep.fft_aligned_sweep('sine_generator.frequency', 
                start_f, end_f, samplerate, sample_count, steps)


        data = sweep.run()

    """

    def __init__(self, testbench, test_type = 'test'):

        ## a valid instance of a configured Testbench class, which is used to 
        # control the hardware and run the acquisitions
        self.testbench = testbench

        ## Optional tag, used to identify what kind of test is being performed by this sweep.
        # This is used for e.g. the database adapter code to properly identify this test,
        # so setting it may be required!
        self.sweep_type = test_type

        ## Ordered list of axes of sweeping options. Each call to sweep() creates a new
        # entry in this list.
        # When run() is called, the code will go through each element in this list in 
        # a recursive call stack, and run through the cross product of each stimulus point.
        #
        # This means that if you have three stimulus_axes entries, each with three points, 
        # the code will call the points in the order 
        # [0][0][0], [0][0][1], [0][0][2], [0][1][0]
        # etc.
        self.stimulus_axes = []

        ## Dict of device values to set as fixed points during the run.
        # Useful for things such as 
        # gain settings, switcher configs, etc., which
        # are relevant to the test but otherwise static
        self.stimulus_setpoints = {}

        ## This structure holds the results of the sweep test. 
        # Each acquisiton, e.g. each combination of sweep points, will get 
        # its own row entry. The order of sweep points is preserved in the data frame
        # call by setting the index property of each row.
        #
        # The columns are as follows:
        # * One column per sweep option settings, e.g. 'sine_generator.frequency', 
        #   'sine_generator.amplitude' and so on
        # * One column per returned key from the Testbench.
        #   Per Testbench convention, this includes all set outputs as 'device.setting', 
        #   and one common entry for measurements called 'measurements', which itself is a dict
        #   of { 'device1' : SomeData(), 'device2' : MoreData() } structure.
        self.result_df = pd.DataFrame()
        
        ## Database related storage for IDs of the result sets
        self.result_ids = []

        ## User-Formatted data.
        # The direct result of the data_eval_fn call
        self.result_data = None

        ## Sweep ID.
        # not used by the code itself but may be used by database code to
        # identify this sweep
        self.sweep_id = None

        ## Optional callback to call for event handling. 
        # Not required for 
        # normal operation, but is typically used as hook to handle saving
        # events to the database
        self.event_handler = None

        self._completed_run_point_count = 0
        self._total_run_point_count = 0

        ## User-provided data evaluation callback.
        # This is an optional callback that can be used to perform calculations on the
        # data *before* the sweep_complete event is fired, e.g. before the data is saved
        # to a database.

        # This function is called immediately after the last sweep point acquisiton 
        # has been performed, and receives, as its only parameter, the completed
        # result_df pandas DataFrame (described above).

        # When this function is set, it MUST return either the result_df frame, or a similar
        # structure, to be saved to the database!
        self.data_eval_fn = None

    def _emit_event(self, data):
        data['test_sweep'] = self
        
        if self.event_handler is not None:
            self.event_handler(data)
        
    def remove_stimulus(self, stimulus_name):
        """Delete a stimulus based on its key.

        Details:
            This function will remove a stimulus from the list 
            based on the key that was used for it (e.g. the 'device.setting' string)
        """

        self.stimulus_axes = list(filter(lambda stim : stim['name'] != stimulus_name, 
                                    self.stimulus_axes))

    def set(self, stimulus_name, stimulus_value):
        """Set a fixed output setting

        Details:
            Using this function will configure the given output to a fixed value when the
            sweep is started (using the Testbench.activate() 
            and testdevice.Device.prepare_outputs()).
            This can be used to perform static configuration of devices for this test.
        """

        self.testbench.assert_output_exists(stimulus_name)
        self.stimulus_setpoints[stimulus_name] = stimulus_value

    def sweep(self, stimulus_name, stimulus_values):
        """Add a new stimulus to sweep over.

        Details:
            Calling this function with a valid stimulus name (e.g. 'device.setting'),
            and an iterable object (list, range, numpy array, etc.) will add 
            those points to the stimulus_axes list.
            When the run() function is called, the sweep will then iterate over 
            every point of the cross product of all sweep() parameters, in the 
            order in which they were provided.

        Note: 
            Only one sweep axis per stimulus name can 
            be provided. If a name is given a second time, the previous entry will be
            removed from the list.
        """

        self.testbench.assert_output_exists(stimulus_name)
        self.remove_stimulus(stimulus_name)

        self.stimulus_axes.append({
            'name': stimulus_name,
            'values': stimulus_values
        })

    def get_setpoints(self):
        """Return a list of the setpoints to be used during this sweep.
        """

        return self.stimulus_setpoints.copy()

    # pylint: disable=too-many-arguments
    def fft_alinged_sweep(self, stimulus_name, 
                start_freq, end_freq, samplerate, sample_size, steps = 50):
        """Sweep function optimized for FFT frequency selection

        Details:
            This function works similarly to sweep(), however instead of providing frequencies, this 
            function will calculate frequency points that fit into the center of frequency bins 
            and are logarithmically spaced.

            For this to work properly, provide a starting and ending frequency, the samplerate, 
            and the total number of sample points (sample_size). 
            sample_size *should* be a power of two for best results.
        """

        frequency_resolution = samplerate / float(sample_size)

        start_freq = max(start_freq, frequency_resolution)

        values = np.logspace(np.log10(start_freq), np.log10(end_freq), steps)
        values /= frequency_resolution

        values  = np.rint(values) 
        values = np.unique(values)
        
        values *= frequency_resolution

        self.sweep(stimulus_name, values)

    def get_total_point_count(self):
        """Return the total amount of acquisitions to be performed by this sweep.
        """

        i = 1
        for stimulus in self.stimulus_axes:
            i *= len(stimulus['values'])

        return i

    def get_completed_point_count(self):
        """Return the count of already completed acquisitions in this sweep.
        """

        return min(self._completed_run_point_count-1, self._total_run_point_count)

     
    def _add_acquisition_result(self, stimuli, acquisition_data):
        """Internal function to add an acquisition result

        Details:
            We need to do a bit of light data reformatting to get things to sit nicely in
            the pandas DataFrame. Mainly, the key-value pairs need to be reshaped into
            key-list pairs, and we need to set the index properly.
        """

        dataframe_data = {}
        for key in stimuli:
            dataframe_data[key] = [stimuli[key]]

        for data_key in acquisition_data:
            dataframe_data[data_key] = [acquisition_data[data_key]]

        # -1 on the index because we start with 1 run, but the data frame index *really* should be
        # zero indexed
        new_df = pd.DataFrame(dataframe_data, index=[self._completed_run_point_count-1])
        self.result_df = pd.concat([self.result_df, new_df])

    def _run_acquisition(self, current_stimuli):

        points_done = self._completed_run_point_count
        points_total = self._total_run_point_count

        acquisition_data, _ = self.testbench.run_acquisition(
            f"\33[2K\rRunning acquisition ({points_done}/{points_total}) "
            f"(setpoints: {current_stimuli})")

        self._add_acquisition_result(current_stimuli, acquisition_data)

        self._emit_event({
            'type': 'test_sweep_point_update', 
            'acquisition_no': self._completed_run_point_count-1,
            'stimuli': current_stimuli.copy(),
            'acquisition': acquisition_data.copy()
        })

        self._completed_run_point_count += 1


    def _run_axis(self, axis, current_stimuli):
        """Recursively iterate through stimulus axes.

        Details:
            This function is built in a recursive fashion of 
            iterating through the stimulus axes.
            It receives the current stimulus axis count (0, 1, etc.).
            If it has reached the end of the list of stimulus axes (e.g.
            axis is outside of self.stimulus_axes), it will perform an 
            acquisition. Otherwise it will run through the 'values' list
            of that stimulus axis, and recursively call itself with the axis
            parameter incremented by one.
        """

        if axis >= (len(self.stimulus_axes)):
            self._run_acquisition(current_stimuli)

            return

        stimulus_axis = self.stimulus_axes[axis]

        for stimulus_value in stimulus_axis['values']:
            self.testbench.set_output(stimulus_axis['name'], stimulus_value)

            next_stimuli = current_stimuli.copy()
            next_stimuli[stimulus_axis['name']] = stimulus_value
            self._run_axis(axis + 1, next_stimuli)

    def _data_processing_step(self, data_frame):
        """Internal function to perform data processing after a finished run

        Details:
            This function is provided both to call the data_eval_fn, if it is provided,
            but is also used as a hook point for inheriting classes to cleanly access 
            the data when an acquisiton has finished.
        """

        if self.data_eval_fn is not None:
            return self.data_eval_fn(data_frame)

        return data_frame

    def run(self):
        """Run through a full sweep of data acquisiton

        Details:
            This function will do the following:
            * Reset the testbench
            * Activate it, setting all devices to the setpoints provided 
              by set() as well as the first point of each stimulus axis
            * Run through each point of the cross product of all points provided by
              sweep(), saving the data into the result_df DataFrame
            * Call the _data_processing_step function to let inheriting classes and
              the TestSweep.data_eval_fn perform needed data processing
            * Ensure that the testbench is reset again before exiting this function.
              This happens even when an error is raised, to improve testbench safety
              and consistency.

            No data is returned. Data should be inspected via the TestSweep.result_df DataFrame 
            and/or the provided callback functions.

            When an error is raised, it is not caught and will be immediately re-raised.
        """

        self._emit_event({'type': 'test_sweep_start'})

        try:
            print("Starting test run!")

            self.testbench.reset()

            time.sleep(2)

            device_init_settings = {}

            for point in self.stimulus_setpoints:
                device_init_settings[point] = self.stimulus_setpoints[point]

            for axis in self.stimulus_axes:
                device_init_settings[axis['name']] = axis['values'][0]

            self.testbench.activate(device_init_settings)

            self._total_run_point_count = self.get_total_point_count()
            self._completed_run_point_count = 1

            self._run_axis(0, {})

            self.result_data = self._data_processing_step(self.result_df)

            print("\nFull test run completed!")

            self._emit_event({ 'type': 'test_sweep_complete' })
        
        except Exception:
            self._emit_event({ 'type': 'test_sweep_failed'})
            raise

        finally:
            self.testbench.reset()
