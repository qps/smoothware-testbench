import time
import random


class Device:
    """Base class for all Testbench devices

    Details:
        This base class provides sane default functions for all testbench
        devices. Testbench devices *must* inherit from this class.

        Functions *may* be implemented as sensible/needed. All base 
        class default functions have sane behaviour.
        Please see the individual function definitions for their documentation.

    Attributes
    ----------
    available_outputs : list
        List of outputs that can be accessed via prepare_outputs and update_outputs. 
        MUST be set by the inheriting class for proper functionality.
    """

    def __init__(self):
        ## List of output keys that this device supports.
        #
        # This *must* be set by inheriting code to a list of
        # output keys that may be present in the calls to 
        # update_outputs().
        self.available_outputs = []

    def activate(self):
        """Activate a device for use in a testbench.

        Details:
            This function may enable a device for operation during a test. This can be used for
            the initial configuration step during which outputs are enabled and relays are closed.
            It is called whenever the Testbench is activated

        @see Testbench.activate
        """

    def reset(self):
        """Reset the device to a known good state.

        Details:
            This function must reset the device to a known-good state, if possible
            deactivating and/or opening all out- and inputs.

            Inheriting code must also take care to reset 
            any internal state back to a sane default!
            Lack of proper reset function will cause "crosstalk" between different tests
            as some settings may bleed over between test runs!!
        
        Warning:
            A lack of proper reset function might cause test crosstalk, which can 
            be hard to debug. Always make sure to properly implement the reset function.
        """

    def has_output(self, key):
        """Checks whether or not a Device has a given output key.

        Details:
            This function will return True/False based on whether a
            given key was found in the available_outputs array. This
            indicates whether or not the device can make use of such
            a key for a update_outputs or prepare_outputs call.
        """
        return key in self.available_outputs

    def prepare_outputs(self, new_values):
        """Configure the device for an upcoming activation.

        Details:
            This function is called during a Testbench
            activation, immediately before activate() is called. It shall be used to
            set up internal values in preparation for the activate() call, and
            to perform first basic setup of the device if necessary.

        Parameters:
            new_values: A dict() in the same format as such for update_outputs,
                to be saved for the activate() call or used immediately.
        
        @see update_outputs
        """

        self.update_outputs(new_values)

    def update_output(self, key, value):
        """Update a single output value of this device.

        Details:
            This function is a thin wrapper around update_outputs(),
            and will construct a dict() with a single key-value pair
            consisting of the given key and value. It is mainly
            meant for convenience.
        """

        set_dict = {}
        set_dict[key] = value
        self.update_outputs(set_dict)

    def update_outputs(self, new_values):
        """Update the outputs of this device.

        Details:
            This function will update the different device
            output parameters as provided in the new_values
            dict. Its exact function depends on the inheriting
            code, however, it shall in general provide an abstract
            interface to the functionality of the respective
            device.

            An example would be a DC Voltage source with one
            parameter, output voltage. It could accept a 
            new_values dict with key 'voltage', and update 
            the voltage according to the contents of this
            parameter.

        Parameters:
            new_values dict() of key-value pairs for output values to update.
        """

    def get_outputs(self):
        """Returns a dict of the current output configuration.

        Details:
            This function shall return a dict with key-value pairs
            of the current output configuration as the device
            sees it.

            For e.g. a DC voltage source with output parameter 
            'voltage', it should return a dict like:
                {
                    'voltage' : 10
                }
            Note that this should not be any *measured* voltage,
            as measurement and acquisition shall be handled by the 
            acquisition_step function!

            May also return None if the output configuration is 
            not important or relevant.
        """
        return None

    def acquisition_step(self, stage):
        """Perform data acquisition and return the measured data.

        Details:
            This function is called from the Testbench.run_acquisition()
            step. Acquisition is performed in discrete stages, each one intended
            for one specific step of the acquisition process to keep
            devices and measurements in sync.
            
            The following stages are run through:
            - prepare
            - trigger
            - wait
            - download
            Devices may use each stage for whatever purpose they require. Only the
            trigger stage *must* be completed as fast as possible, to allow 
            all test devices to be triggered as simultaneously as feasible.

            Only the download() stage may return data. Any returned data
            is added to the recorded data of the Testbench, under the
            measurements dict.

            @see Testbench.run_acquisition()
        """

        if stage == 'download':
            return self.get_outputs()
        
        return None

class MockTestDevice(Device):
    """Mockup class for internal testing purposes.

    Details:
        This function acts like a functional, fully simulated 
        version of a Device. Its main purpose is to allow 
        for the testing of other code functionality, by pretending
        to be a device (either with generic outputs and/or with 
        fixed output data).
    """

    def __init__(self, return_data=None, download_time=0.2, failure_percentage=0):
        super().__init__()

        self.download_time = download_time
        self.return_data = return_data
        if self.return_data is None:
            self.return_data = {}

        self.mock_failure_threshold = failure_percentage

        self.output_data = {}

    def update_outputs(self, new_values):
        time.sleep(0.1)
        self.output_data.update(new_values)

    def get_outputs(self):
        return self.output_data.copy()

    def acquisition_step(self, stage):
        if stage != 'download':
            return None

        time.sleep(self.download_time)

        if random.random() < self.mock_failure_threshold:
            raise RuntimeError("Test error!")

        return self.return_data
