import json

from smoothwaretb.database.testbench_db_handler import TestbenchDBHandler
from smoothwaretb.database.testbench_db_handler import pgretry


class UqdsDBHandler(TestbenchDBHandler):
    """UQDS-Specific Database Handler

    Details:
        This class expands on the existing database system by adding uQDS-Specific
        measurement tables.

        Mainly, it creates tables to identify uQDS Batches, individual cards, 
        and per-card results (whereas the testbench code has up to 16 cards in a single 
        result)

        It also provides a convenience materialized view that provides the latest
        result type for each given card.
    """
    def __init__(self, pg):
        super().__init__(pg)

        self.batch_id = None
        self.channel_ids = []
        self.channel_result_ids = []

    @pgretry
    def _DROP_SCHEMA(self):
        raise RuntimeError("DISABLED FUNCTION - please don't call me!")

        #super()._DROP_SCHEMA()

        #with self.pg:
        #    with self.pg.cursor() as cursor:
        #        cursor.execute("DROP TABLE IF EXISTS uqds_channel_batches CASCADE")
        #        cursor.execute("DROP TABLE IF EXISTS uqds_channels CASCADE")
        #        cursor.execute("DROP TABLE IF EXISTS uqds_channel_results CASCADE")
        #        cursor.execute("DROP TABLE IF EXISTS uqds_channel_acquisitions CASCADE")

    @pgretry
    def setup_schema(self):
        super().setup_schema()

        with self.pg:
            with self.pg.cursor() as cursor:
                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS uqds_channel_batches (
                        uqds_channel_batch_id SERIAL PRIMARY KEY,
                        batch_key TEXT UNIQUE NOT NULL,
                        batch_total_channels INTEGER DEFAULT 0
                    )
                """)

                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS uqds_channels (
                        uqds_channel_sql_id SERIAL PRIMARY KEY,
                        uqds_channel_id TEXT NOT NULL,
                        uqds_channel_batch_id INTEGER REFERENCES uqds_channel_batches ( uqds_channel_batch_id ) ON DELETE CASCADE,
                        channel_type TEXT NOT NULL,
                        creation_time TIMESTAMPTZ DEFAULT NOW()
                    )
                """)

                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS uqds_channel_results (
                        uqds_channel_result_id SERIAL PRIMARY KEY,
                        uqds_channel_sql_id INTEGER REFERENCES uqds_channels ( uqds_channel_sql_id ) ON DELETE CASCADE,
                        testbench_sweep_id INTEGER REFERENCES testbench_sweeps ( testbench_sweep_id ) ON DELETE CASCADE,
                        channel_pga_setting INTEGER NOT NULL DEFAULT 1,
                        result_failing BOOLEAN NOT NULL DEFAULT FALSE,
                        result_type TEXT NOT NULL DEFAULT 'test',
                        result_version INTEGER DEFAULT NULL,
                        result_data JSONB COMPRESSION PGLZ DEFAULT NULL,
                        result_completed_time TIMESTAMPTZ DEFAULT NULL
                    )
                """)

                cursor.execute("""
                    CREATE MATERIALIZED VIEW IF NOT EXISTS uqds_channel_results_latest AS
                    WITH known_types (result_type) AS (
                         VALUES ('thd') , 
                            ('cmrr'),
                            ('bandwidth'),
                            ('linearity'),
                            ('linearity_cal'),
                            ('noise')
                    ),
                    known_pga_settings (channel_pga_setting) AS (
                        VALUES (1), (9), (45), (450)
                    ),
                    full_test_list AS (
                        SELECT uqds_channel_sql_id, result_type, channel_pga_setting
                        FROM uqds_channels
                        CROSS JOIN known_types
                        CROSS JOIN known_pga_settings
                        WHERE CASE result_type WHEN 'noise' THEN channel_pga_setting = 1 ELSE TRUE END
                    ),
                    result_ids AS (
                        SELECT min(result_completed_time) AS earliest_result, 
                            max(uqds_channel_result_id) AS uqds_channel_result_id
                        FROM uqds_channel_results
                        WHERE result_version IS NOT NULL
                        GROUP BY uqds_channel_sql_id, result_type, channel_pga_setting
                    )
                    SELECT *, result_data IS NOT NULL as result_present
                    FROM uqds_channel_results
                    INNER JOIN result_ids USING (uqds_channel_result_id)
                    INNER JOIN uqds_channels USING ( uqds_channel_sql_id )
                    INNER JOIN uqds_channel_batches USING ( uqds_channel_batch_id )
                    FULL OUTER JOIN full_test_list USING ( uqds_channel_sql_id, result_type, channel_pga_setting )
                    WHERE batch_key LIKE 'EDA%'
                """)
                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS uqds_channel_acquisitions (
                        uqds_channel_acquisition_id INTEGER NOT NULL,
                        uqds_channel_result_id INTEGER REFERENCES uqds_channel_results ( uqds_channel_result_id ) ON DELETE CASCADE,
                        measurements JSONB COMPRESSION PGLZ DEFAULT '{}',
                        timestep DOUBLE PRECISION NOT NULL,
                        buffer_data DOUBLE PRECISION[],
                        fft_data DOUBLE PRECISION[],
                        PRIMARY KEY ( uqds_channel_result_id, uqds_channel_acquisition_id )
                    )
                """)

    @pgretry
    def setup_channels(self, batch, channel_list, channel_types):
        """Set channels up with database IDs.

        Details:
            This function *must* be called before the test collection/test
            sweeps are started. It will attempt to either create or fetch 
            the channel identifications from the database.

            These IDs are saved locally in the channel_ids array, and are
            used during later testbench acquisition events to properly 
            associate measurement results with channels.
        
        Parameters:
            batch : Text, name of the batch or group of channels.
            channel_list : list of text, IDs of the channels (usually "0241" or similar)
            channel_types : list of text, types of the channels (usually "normal")

        Todo:
            Split batch up into production_batch and channel_design.
            We might want to both track differences between production runs 
            as well as channel designs, which can't be done here yet.
        """
        with self.pg:
            with self.pg.cursor() as cursor:

                cursor.execute("SELECT uqds_channel_batch_id FROM uqds_channel_batches "
                               "WHERE batch_key = %s",
                               [batch])
                if cursor.rowcount == 0:
                    cursor.execute \
                        ("INSERT INTO uqds_channel_batches (batch_key)" 
                         "VALUES (%s) RETURNING uqds_channel_batch_id",
                         [batch])
                self.batch_id = cursor.fetchall()[0][0]

                self.channel_ids = []
                for i in enumerate(channel_list):
                    channel = channel_list[i]
                    cursor.execute("SELECT uqds_channel_sql_id "
                                   "FROM uqds_channels "
                                   "WHERE uqds_channel_id = %s AND uqds_channel_batch_id = %s",
                                   [str(channel), self.batch_id])
                    if cursor.rowcount == 0:
                        cursor.execute("""
                            INSERT INTO uqds_channels (uqds_channel_id, uqds_channel_batch_id, channel_type) 
                            VALUES ( %s, %s, %s) 
                            RETURNING uqds_channel_sql_id
                        """, [str(channel), self.batch_id, channel_types[i]])
                    else:
                        print(f"Channel ID {channel} already exists in DB"
                               " - are you re-testing a channel?")

                    self.channel_ids.append(cursor.fetchall()[0][0])

    @pgretry
    def _save_channel_results(self, event_data):
        sweep = event_data['test_sweep']
        channel_results = sweep.result_data

        with self.pg:
            with self.pg.cursor() as cursor:
                for i in enumerate(channel_results.channels):
                    channel = channel_results.channels[i]
                    result_id = self.channel_result_ids[i]

                    cursor.execute("""
                        UPDATE uqds_channel_results
                        SET result_data = %s,
                            result_failing = %s,
                            result_completed_time = NOW()
                        WHERE uqds_channel_result_id = %s
                    """, [json.dumps(channel.result_data), channel.result_failed, result_id])

                cursor.execute("REFRESH MATERIALIZED VIEW uqds_channel_results_latest")

    @pgretry
    def _channel_sweep_start(self, event_data):
        sweep = event_data['test_sweep']

        with self.pg:
            with self.pg.cursor() as cursor:
                self.channel_result_ids = []

                for i in enumerate(self.channel_ids):
                    channel_id = self.channel_ids[i]

                    cursor.execute("""
                        INSERT INTO uqds_channel_results (
                            uqds_channel_sql_id,
                            testbench_sweep_id,
                            channel_pga_setting,
                            result_type )
                        VALUES ( %s, %s, %s, %s )
                        RETURNING uqds_channel_result_id
                    """, [channel_id, sweep.sweep_id, sweep.uqds_pga_setting, sweep.sweep_type])

                    self.channel_result_ids.append(cursor.fetchall()[0][0])

    @pgretry
    def _testbench_sweep_update(self, event_data):

        acquisition_data = event_data['acquisition']

        acquisition_data = dict(acquisition_data)
        acquisition_data['measurements'] = dict(acquisition_data['measurements'])
        acquisition_data['measurements']['uqds'] = dict(acquisition_data['measurements']['uqds'])

        uqds_channels = acquisition_data['measurements']['uqds']['channels']

        del acquisition_data['measurements']['uqds']['channels']

        event_data['acquisition'] = acquisition_data

        super()._testbench_sweep_update(event_data)

        with self.pg:
            with self.pg.cursor() as cursor:
                for ch_index in enumerate(uqds_channels):
                    ch = uqds_channels[ch_index] # pylint: disable=invalid-name
                    cursor.execute("""
                        INSERT INTO uqds_channel_acquisitions (
                            uqds_channel_acquisition_id,
                            uqds_channel_result_id,
                            measurements,
                            timestep
                        )
                        VALUES ( %s, %s, %s, %s )
                    """, [event_data['acquisition_no'], self.channel_result_ids[ch_index],
                          json.dumps(ch.measurements), ch.timestep])

    def handle_testbench_event(self, event_data):
        super().handle_testbench_event(event_data)

        et = event_data['type']  # pylint: disable=invalid-name

        if et == 'test_sweep_complete':
            self._save_channel_results(event_data)
        elif et == 'test_sweep_start':
            self._channel_sweep_start(event_data)
