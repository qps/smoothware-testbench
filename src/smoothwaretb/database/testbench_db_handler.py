
import json
import time

from functools import wraps

import psycopg2


def pgretry(pg_function):
    """Postgres auto-retry decorator.

    Details:
        This decorator will capture the psycopg2 connection errors, and
        will gracefully try to reconnect and then retry the function.
    
    Note:
        This may retry the function in a try-catch block. Make sure the function
        does not mutate other state, else the retrying might clobber that 
        state further!
    """
    @wraps(pg_function)
    def wrapper(*args, **kw): # pylint: disable=inconsistent-return-statements
        cls = args[0]
        # pylint: disable=protected-access
        for _ in range(cls._reconnectTries):
            try:
                return pg_function(*args, **kw)
            except (psycopg2.InterfaceError, psycopg2.OperationalError):
                print ("\nDatabase Connection [InterfaceError or OperationalError]")
                print (f"Idle for {cls._reconnectIdle} seconds")
                time.sleep(cls._reconnectIdle)
                cls._connect()
    return wrapper

class TestbenchDBHandler:
    """Testbench to Database handler class.

    Details:
        This class is meant to connect a generic test_sweep.TestSweep instance
        to a PostgreSQL database.

        It uses the test_sweep.TestSweep.event_handler and 
        test_collection.TestCollection.db_adapter hooks to grab data and 
        events from the tests as they happen, and will save them to the database.

        It also handles creating the database schema on a fresh database.
        It does *not yet* handle updating and maintaining the database schema!

        The pg_params parameter shall be a dict containing the connection details 
        to pass on to the psycopg2 class to connect. This class will handle
        connecting as well as reconnect-attempts.

        The following events are caught and handled:
        * Starting a Testbench collection via test_collection.TestCollection.run()
            * This will add a new entry to the testbench_collections table,
            * and will additionally add new entries to testbench_sweeps for all
              sweeps known to the collection.
        * Starting a new test sweep.
            * This will update a pre-exisiting testbench_sweeps row
        * Saving a new test sweep acquisition
            * This will update the preexisting collection and sweep rows, and
            * add a new row to the testbench_acquisitions table
        * As well as sweep and collection completions or failures

        For more details on the database schema, see setup_schema.

    Note: 
        Database schema migrations/upgrades are not yet handled by this code. If a new version
        of code with modified schema is brought out, the user must take care to manually
        modify the database schema as needed!
    
    Todo:
        Implement proper database migration handling.
    """
    def __init__(self, pg_params):
        # pylint: disable=invalid-name
        self.pg = None
        self.pg_params = pg_params

        self._reconnectIdle = 0.2
        self._reconnectTries = 5

        self._connect()

        self.testbench_collection_id = None

        self.setup_schema()

    def _connect(self):
        try:
            self.pg.close()
        # Ignoring *all* exceptions because we really don't care
        # if the disconnect worked or is needed.
        except: # pylint: disable=bare-except
            pass

        self.pg = psycopg2.connect(**self.pg_params)

    @pgretry
    def _DROP_SCHEMA(self): # pylint: disable=invalid-name
        raise RuntimeError("DISABLED FUNCTION - please don't call me!")

        #with self.pg:
        #    with self.pg.cursor() as cursor:
        #        cursor.execute("DROP TABLE IF EXISTS testbench_collections CASCADE")
        #        cursor.execute("DROP TABLE IF EXISTS testbench_sweeps CASCADE")
        #        cursor.execute("DROP TABLE IF EXISTS testbench_acquisitions CASCADE")

    @pgretry
    def setup_schema(self):
        # pylint: disable=line-too-long
        """Set up the database schema of a new database.

        Details:
            This function will set up a blank database with a 
            fresh schema.

            The table schema is kept simple and closely related 
            to the available python classes of the testbench code, 
            in the following structure:

            * testbench_collections represents test_collection.TestCollection
            * testbench_sweeps represents test_sweep.TestSweep
            * testbench_acquisitions represent the individual aquisiton points of a
                TestSweep. These do not have a python class, and instead represent
                the individual rows of the Result Dataframe of the TestSweep.
            
            The tables are linked via primary keys/foreign keys to ensure a 
            valid data schema. The data of the measurements itself is stored
            in the testbench_acquisitions table, where the stimuli of the 
            acquisition are stored in the stimuli JSONB 
            (see testbench.Testbench.get_output_values_flattened()), and 
            the measurements of the acquisition are stored in the measurements JSONB.

            The tables are as follows:

            <h3>testbench_collections</h3>

            | Column | Type | Description |
            |  ----  | ---  | ---- |
            | testbench_collection_id | SERIAL | Automatic serial primary key to link with other tables |
            | create_time | TIMESTAMPTZ | Creation time of the row |
            | last_update | TIMESTAMPTZ | Last update time of the row |
            | state | TEXT | Textual state. May be 'running', 'completed' or 'failed' |
            | total_acquisition_count | INTEGER | Number of acquisitons to complete in total |
            | completed_acquisiton_count | INTEGER | Number of acquisitions of all sweeps that have already been completed. |
            
            <h3>testbench_sweeps</h3>

            | Column | Type | Description |
            |  ----  | ---  | ---- |
            | testbench_sweep_id | SERIAL | Primary key of the column |
            | testbench_collection_id | INTEGER | Foreign key, linking to the testbench_collection column |
            | create_time | TIMESTAMPTZ | Time at which this sweep was *created*, not necessarily *started* |
            | last_update | TIMESTAMPTZ | Time of the last update/change to this column. |
            | state | TEXT | State of this sweep. Will be 'not started', 'running', 'completed' or 'failed' |
            | type  | TEXT | Type of this sweep. Depends on the setting of the sweep_type parameter, indicates the type of test that was performed. |
            | acquisition_count | INTEGER | Total number of acquisitions to perform for this sweep |
            | completed_acquisition_count | INTEGER | Number of acquisitions that have been successfully performed so far. |
            | setpoints | JSONB | JSON structure of the setpoints of the sweep (see test_sweep.TestSweep.set()) |
        
            <h3>testbench_acquisitions</h3>

            | Column | Type | Description |
            |  ----  | ---  | ---- |
            | testbench_acquisition_id | INTEGER | Part of the primary key (together with testbench_sweep_id), indicating which acquisition this is. |
            | testbench_sweep_id | INTEGER | Foreign key to the testbench_sweeps table, and part of the primary key of this table. |
            | completion_time | TIMESTAMPTZ | Time at which this acquisition was performed |
            | stimuli | JSONB | JSON of the applied stimuli/outputs of the testbench (see testbench.Testbench.get_output_values_flattened()) | 
            | measurements | JSONB | JSON of the captured data. Directly saved from the testbench.Testbench.run_acquisition() measurements element |
        
        Todo:
            Add a column for the start time of a testbench sweep.

        """
        with self.pg:
            with self.pg.cursor() as cursor:
                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS testbench_collections (
                        testbench_collection_id SERIAL PRIMARY KEY NOT NULL,
                        create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
                        last_update TIMESTAMPTZ DEFAULT NOW(),
                        state TEXT DEFAULT 'running',
                        total_acquisition_count INTEGER NOT NULL,
                        completed_acquisition_count INTEGER DEFAULT 0
                    )
                """)

                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS testbench_sweeps (
                        testbench_sweep_id SERIAL PRIMARY KEY NOT NULL,
                        testbench_collection_id INTEGER REFERENCES testbench_collections ( testbench_collection_id ) ON DELETE CASCADE,
                        create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
                        last_update TIMESTAMPTZ DEFAULT NOW(),
                        state TEXT DEFAULT 'not started',
                        type TEXT,
                        acquisition_count INTEGER NOT NULL,
                        completed_acquisition_count INTEGER DEFAULT 0,
                        setpoints JSONB COMPRESSION PGLZ DEFAULT '{}'
                    )
                """)

                cursor.execute("""
                    CREATE TABLE IF NOT EXISTS testbench_acquisitions (
                        testbench_acquisition_id INTEGER NOT NULL,
                        testbench_sweep_id INTEGER REFERENCES testbench_sweeps ( testbench_sweep_id ) ON DELETE CASCADE,
                        completion_time TIMESTAMPTZ DEFAULT NOW(),
                        stimuli JSONB DEFAULT '{}',
                        measurements JSONB COMPRESSION PGLZ DEFAULT NULL,
                        PRIMARY KEY ( testbench_sweep_id, testbench_acquisition_id )
                    )
                """)
 
    @pgretry
    def _testbench_collection_setup(self, collection):
        with self.pg:
            with self.pg.cursor() as cursor:
                cursor.execute("""
                    INSERT INTO testbench_collections
                        (
                            total_acquisition_count
                        )
                    VALUES ( %s )
                    RETURNING testbench_collection_id
                """, [collection.get_total_point_count()])

                collection.testbench_collection_id = cursor.fetchall()[0][0]

    @pgretry
    def _testbench_collection_completed(self, event_data):
        with self.pg:
            with self.pg.cursor() as cursor:
                collection = event_data['test_collection']

                cursor.execute("""
                    UPDATE testbench_collections
                    SET last_update = NOW(), state = 'completed'
                    WHERE testbench_collection_id = %s
                """, [collection.testbench_collection_id])

    @pgretry
    def _testbench_sweep_setup(self, event_data):
        with self.pg:
            with self.pg.cursor() as cursor:
                sweep = event_data['test_sweep']
                collection = event_data['test_collection']

                cursor.execute("""
                    INSERT INTO testbench_sweeps (
                        testbench_collection_id,
                        type, 
                        acquisition_count,
                        setpoints
                    ) 
                    VALUES ( %s, %s, %s, %s )
                    RETURNING testbench_sweep_id
                """, [collection.testbench_collection_id, 
                    sweep.sweep_type, 
                    sweep.get_total_point_count(), 
                    json.dumps(sweep.get_setpoints())])

                sweep.sweep_id = cursor.fetchall()[0][0]

    @pgretry
    def _testbench_sweep_update(self, event_data):
        with self.pg:
            with self.pg.cursor() as cursor:
                sweep = event_data['test_sweep']
                collection = event_data['test_collection']

                cursor.execute("""
                    UPDATE testbench_collections
                    SET last_update = NOW(), completed_acquisition_count = %s
                    WHERE testbench_collection_id = %s
                """, [collection.get_completed_point_count(), collection.testbench_collection_id])

                cursor.execute("""
                    UPDATE testbench_sweeps
                    SET last_update = NOW(), 
                        completed_acquisition_count = %s
                    WHERE testbench_sweep_id = %s
                """, [sweep.get_completed_point_count(), sweep.sweep_id])

                cursor.execute("""sweep._completed_run_point_count
                    INSERT INTO testbench_acquisitions (
                        testbench_sweep_id,
                        testbench_acquisition_id,
                        completion_time,
                        stimuli,
                        measurements
                    )
                    VALUES ( %s, %s, NOW(), %s, %s )
                    RETURNING testbench_acquisition_id
                """, [  sweep.sweep_id,
                        event_data['acquisition_no'],
                        json.dumps(event_data['stimuli']),
                        json.dumps(event_data['acquisition'])])

    @pgretry
    def _testbench_sweep_state(self, sweep, state):
        with self.pg:
            with self.pg.cursor() as cursor:
                cursor.execute("""
                    UPDATE testbench_sweeps
                    SET state = %s
                    WHERE testbench_sweep_id = %s
                """, [state, sweep.sweep_id])

    def handle_testbench_event(self, event_data):
        """Base function to handle database events.

        Details:
            This function takes in events from the Testbench code, and will 
            save or otherwise connect the bench code results with the database
            backend. 
            This is done by reacting in specific ways to predefined testbench events.
        """

        # pylint: disable=invalid-name
        et = event_data['type']

        if et == 'test_collection_start':
            self._testbench_collection_setup(event_data['test_collection'])
        elif et == 'test_sweep_setup':
            self._testbench_sweep_setup(event_data)
        elif et == 'test_sweep_start':
            self._testbench_sweep_state(event_data['test_sweep'], 'running')
        elif et == 'test_sweep_point_update':
            self._testbench_sweep_update(event_data)
        elif et == 'test_sweep_complete':
            self._testbench_sweep_state(event_data['test_sweep'], 'completed')
        elif et == 'test_sweep_failed':
            self._testbench_sweep_state(event_data['test_sweep'], 'failed')
        elif et == 'test_collection_complete':
            self._testbench_collection_completed(event_data)
        else:
            raise RuntimeError("Unknown event type received!")
