
import re

from smoothwaretb.test_sweep import TestSweep
from smoothwaretb.device_wrappers import uqds

class UqdsTestSweep(TestSweep):
    """UQDS-Specific sweep implementation


    Details:
        This class provides a specialisation of the Test Sweep class,
        with helper functions defined around dealing with UQDS Setups, 
        as well as hooks into the data processing pipeline that properly 
        extract per-channel data.

        It provides functions that do well-configured frequency and voltage
        sweeps, and parse the uQDS Channel data output with these in mind.
    """

    # pylint: disable=too-many-arguments
    def __init__(self, testbench, uqds_key='uqds', 
            sample_count=32768, channel_count=16, pga_setting=1):

        super().__init__(testbench)

        self.uqds_key = uqds_key
        self.uqds_sample_count = sample_count
        self.uqds_channel_count = channel_count
        self.uqds_pga_setting = pga_setting

        self.set(uqds_key + '.sample_count', sample_count)
        self.set(uqds_key + '.channel_count', channel_count)
        self.set(uqds_key + '.pga_setting', pga_setting)

        self.uqds_stimulus_frequency = None
        self.uqds_stimulus_amplitude = None

        self.uqds_save_linearity_values = False
        self.uqds_save_histogram = False

        self.calculate_3db_point = False
        self.linearity_calc_reference_key = None
        self.statistics_eval_limits = {}

    def uqds_fft_sweep(self, freq_stimulus, amplitude_stimulus,
            start_freq, end_freq, amplitude=10, steps=50):
        """Perform a FFT sweep and parse uQDS Channel data accordingly.

        Details:
            This function is a specialisation of the existing FFT sweep
            helper, with the added benefit of enabling uQDS-Channel-Specific
            post-processing options.
        """
        self.uqds_stimulus_frequency = freq_stimulus
        self.uqds_stimulus_amplitude = amplitude_stimulus

        self.set(amplitude_stimulus, amplitude)
        self.fft_alinged_sweep(freq_stimulus, start_freq, end_freq,
            615384.0, self.uqds_sample_count, steps)

    def uqds_linearity_sweep(self, stimulus, dcmeter, voltages, save_calibration=False):
        """Perform a linear sweep and parse uQDS Channel data accordingly.

        Details:
            This function is a specialisation of the existing linear sweep
            helper, with the added benefit of enabling uQDS-Channel-Specific
            post-processing options, as well as dealing with calibration data.
        """

        self.linearity_calc_reference_key = dcmeter
        self.uqds_save_linearity_values = save_calibration
        self.sweep(stimulus, voltages)

    def _add_acquisition_result(self, stimuli, acquisition_data):
        if self.uqds_stimulus_frequency is not None:
            stim_freq = acquisition_data[self.uqds_stimulus_frequency]
            stim_amplitude = acquisition_data[self.uqds_stimulus_amplitude]

            for channel in acquisition_data['measurements'][self.uqds_key]['channels']:
                channel.set_stimulus(stim_freq, stim_amplitude)

        if self.linearity_calc_reference_key is not None:
            key = re.search(r"^([^\.]+)\.(.*)$", self.linearity_calc_reference_key)

            reference_voltage = acquisition_data['measurements'][key.group(1)][key.group(2)]

            for ch in acquisition_data['measurements'][self.uqds_key]['channels']:
                ch.measurements['stimulus_voltage'] = reference_voltage

                stimulus_diff = ch.measurements['average'] - reference_voltage
                ch.measurements['avg_to_stimulus_difference'] = stimulus_diff

        if self.uqds_save_histogram:
            for ch in acquisition_data['measurements'][self.uqds_key]['channels']:
                ch.calculate_histogram()

        super()._add_acquisition_result(stimuli, acquisition_data)

    def evaluate_data(self, calculate_3db_point=False,
                      calculate_linearity_along=None,
                      save_histogram=False,
                      statistics=None):
        """Postprocessing-Configuration function

        Details:
            This function is used to configure data postprocessing
            when setting up the uQDS Sweep.
            Mainly, it is to be used to enable/disable features, but
            for the linearity parameter the user must supply a device 
            key for a reference voltage.
        """

        if calculate_3db_point:
            self.calculate_3db_point = True

        if calculate_linearity_along is not None:
            self.linearity_calc_reference_key = calculate_linearity_along

        if save_histogram:
            self.uqds_save_histogram = True

        if statistics is not None:
            for key in statistics:
                self.statistics_eval_limits[key] = statistics[key]

    def _data_processing_step(self, data_frame):
        wrapper = uqds.UqdsSweepResultHandler(data_frame)

        if self.uqds_stimulus_frequency is not None:
            wrapper.configure_stimulus_sine(self.uqds_stimulus_frequency,
                self.uqds_stimulus_amplitude)

        if self.calculate_3db_point:
            wrapper.calculate_3db_points()

        if self.linearity_calc_reference_key is not None:
            wrapper.calculate_linearity(self.linearity_calc_reference_key)

        if self.uqds_save_linearity_values:
            ch_gains = []
            ch_offsets = []
            for ch in wrapper.channels:
                lin_data = ch.result_data['linearity']
                ch_gains.append(lin_data['a'])
                ch_offsets.append(lin_data['b'])

            self.testbench.devices[self.uqds_key].setup_channel_calibrations(
                ch_gains, ch_offsets
            )

        wrapper.evaluate_stats(self.statistics_eval_limits)

        return super()._data_processing_step(wrapper)
