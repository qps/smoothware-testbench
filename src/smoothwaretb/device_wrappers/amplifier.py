from smoothwaretb.testdevice import Device


class AmplifierDevice(Device):
    """Amplifier Device implementation

    Details:
        This class implements an interface to an analog x1/x5 amplifier and switch.

        This unit allows a channel selection between the two input channels,
        as well as an enable/disable switch for the amplification.
    """

    def __init__(self, amplifier):
        super().__init__()

        self.amp = amplifier
        self.output_channel_select = None
        self.amp_enable = True

        self.available_outputs = [
            'channel',
            'amp_enabled'
        ]

    def reset(self):
        self.amp.reset_device()

        self.amp.set_output_off(1)
        self.amp.set_output_off(2)

        self.output_channel_select = None
        self.amp_enable = True

    def prepare_outputs(self, new_values):
        if 'amp_enabled' in new_values:
            self.amp_enable = new_values['amp_enabled']
        if 'channel' in new_values:
            self.output_channel_select = new_values['channel']

    def activate(self):
        if self.output_channel_select is not None:
            if self.amp_enable:
                self.amp.set_amplification(self.output_channel_select, 2, 2)
            else:
                self.amp.set_direct_path(self.output_channel_select, 2, 2)

            self.amp.set_output_on(self.output_channel_select)
