
import time

from smoothwaretb.testdevice import Device

class DCMeterDevice(Device):
    """Precision DC-Meter class implementation

    Details:
        This class implements an interface to precision
        DC-Meters during the testbench aquisition.

        The DC-Meter must be enabled with the `enabled` device 
        key. Additionally, a voltage range can be selected 
        via the `voltage_range` key.

        The default voltage range is `200V`.

        Once enabled, the DC Meter will perform two aquisitions, 
        one during the prepare step to let the DC-Meter in question
        acclimatise, and one measurement during the `wait` step
        immediately after the actual aquistion (to let the uQDS Crate
        be recording and avoid a time-delay between it and the DC-Meter)
    """
    def __init__(self, meter):
        super().__init__()

        self.meter = meter
        self.enabled = False

        self.voltage = 0
        self.voltage_range = 200

        self.available_outputs = [
            'enabled',
            'voltage_range'
        ]

    def prepare_outputs(self, new_values):
        if 'enabled' in new_values:
            self.enabled = new_values['enabled']

        if 'voltage_range' in new_values:
            self.voltage_range = new_values['voltage_range']

    def activate(self):
        if self.enabled:
            self.meter.prime_for_meas(manual_trigger=True, range_volt=self.voltage_range)

    def reset(self):
        self.enabled = False
        self.meter.reset_device()

    def acquisition_step(self, stage):
        if not self.enabled:
            return None

        if stage == 'prepare':
            self.meter.measure_dc_voltage(range_volt=self.voltage_range,
                                          integr_type=4)
            time.sleep(10)

        if stage == 'wait':
            self.voltage, _ = self.meter.measure_dc_voltage(range_volt=self.voltage_range,
                                                             integr_type=4)

        if stage == 'download':
            return {
                'voltage': float(self.voltage)
            }
        
        return None
