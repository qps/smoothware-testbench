import time

from smoothwaretb.testdevice import Device

class MatrixDevice(Device):
    """Matrix switching device implementation

    Details:
        This class implements a basic interface 
        for an analog switching//interconnect matrix.

        It is tailored to the uQDS Testbench and may not
        work with all setups. It assumes a specific I/O
        setup and will interconnect four inputs to all sixteen 
        output channels based on the `source` parameter.
    """

    def __init__(self, matrix):
        super().__init__()

        self.matrix = matrix
        self.source_select = None
        self.hold_source = False

        self.switch_wait_time = None

        self.available_outputs = [
            'source',
            'hold_source',
            'switch_wait_time'
        ]

    def reset(self):
        self.source_select = None
        self.switch_wait_time = None

        if not self.hold_source:
            for j in range(4):
                for i in range(16):
                    self.matrix.channel_routing_uqds_off(j+1, i + 1)

    def prepare_outputs(self, new_values):
        if 'source' in new_values:
            self.source_select = new_values['source']

        if 'hold_source' in new_values:
            self.hold_source = new_values['hold_source']
        else:
            self.hold_source = False

        if 'switch_wait_time' in new_values:
            self.switch_wait_time = new_values['switch_wait_time']

    def activate(self):
        if self.source_select is None:
            return

        if self.source_select > 4 or self.source_select < 1:
            raise RuntimeError(f"Matrix source {self.source_select} does not exist!")

        for j in range(4):
            if j+1 != self.source_select:
                for i in range(16):
                    self.matrix.channel_routing_uqds_off(j+1, i + 1)

        for i in range(16):
            self.matrix.channel_routing_uqds_on(self.source_select, i+1)

        if self.switch_wait_time is not None:
            time.sleep(self.switch_wait_time)
