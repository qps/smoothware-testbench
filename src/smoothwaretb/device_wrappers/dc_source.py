
import time

from smoothwaretb.testdevice import Device

class DCSourceDevice(Device):
    """DC-Source test device implementation

    Details:
        This class expands on the generic device class to add
        a DC output option.

        This DC output uses the device key `voltage`.
        When the voltage is set, the given `source` 
        object's `set_dc_voltage` function will be called to set the output voltage.

        Upon reset, the `reset_device()` function will be called.
    """
    def __init__(self, source):
        super().__init__()

        self.device = source
        self.current_voltage = 0

        self.postset_sleep_time = 2

        self.available_outputs = [
            'voltage'
        ]

    def reset(self):
        self.current_voltage = 0
        self.device.reset_device()
        self.device.set_output_zero()

    def update_outputs(self, new_values):
        if 'voltage' in new_values:
            next_voltage = new_values['voltage']

            ret = self.device.set_dc_voltage(next_voltage)
            if ret != 1:
                raise RuntimeError("Device did not update voltage!")
            self.current_voltage = float(next_voltage)

            time.sleep(self.postset_sleep_time)

    def get_outputs(self):
        return {
            'voltage': self.current_voltage
        }
