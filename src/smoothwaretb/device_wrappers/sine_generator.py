
import time

from smoothwaretb.testdevice import Device

class SineDevice(Device):
    """Sine function generator device

    Details:
        This device implements a generic wave-function generator
        interface, tailored to sine-wave generation.

        It exposes the keys `offset`, `frequency` and
        `amplitude`, which should be self-explanatory for a sine-wave.
    """

    def __init__(self, device, channel=1, amplification=1, switch_wait_time=0.1):
        super().__init__()

        self.device_handler = device
        self.device_channel = channel

        self.switch_wait_time = switch_wait_time

        self.amplification = amplification

        self.current_values = {
            'offset': 0,
            'amplitude': 0,
            'frequency': 0
        }

        self.available_outputs = [
            'channel',
            'amplitude',
            'offset',
            'frequency'
        ]

    def reset(self):
        self.current_values = {
            'offset': 0,
            'amplitude': 0,
            'frequency': 0
        }

        self.device_channel = 1
        self.device_handler.reset_device()

    def activate(self):
        if self.current_values['frequency'] <= 0:
            return

        fgen = self.device_handler

        fgen.reset_device()
        if callable(getattr(fgen, 'set_high_impedance', None)):
            fgen.set_high_impedance(self.device_channel)
        if callable(getattr(fgen, 'set_termination', None)):
            fgen.set_termination(3)

        self.update_outputs({})

        if callable(getattr(fgen, 'set_output_on', None)):
            fgen.set_output_on(self.device_channel)

        time.sleep(0.3)

    def prepare_outputs(self, new_values):
        if 'channel' in new_values:
            self.device_channel = new_values['channel']

        self.current_values.update(new_values)

    def update_outputs(self, new_values):
        self.current_values.update(new_values)

        self.device_handler.set_sine(self.current_values['frequency'],
                                     self.current_values['amplitude'] / self.amplification,
                                     self.current_values['offset'],
                                     channel=self.device_channel)

        time.sleep(self.switch_wait_time)

    def get_outputs(self):
        return self.current_values.copy()
