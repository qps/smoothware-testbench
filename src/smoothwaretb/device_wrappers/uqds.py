import time
import re
import sys

import numpy as np
import numpy.linalg

from smoothwaretb.testdevice import Device
from smoothwaretb.bufferwrapper import BufferWrapper

class MockUQDSDevice(Device):
    """Mock uQDS Device to generate fake testing data
    """

    def __init__(self):
        super().__init__()

        self.current_settings = {
            'sample_count': 32768,
            'channel_count': 4,
            'samplerate': 400000,
            'mock_stim_freq': 0,
            'mock_stim_triangle': 1e-3,
            'mock_stim_amplitude': 1,
            'mock_stim_noise': 1e-3
        }

        self.available_outputs = [
            "channel_count",
            "sample_count",

            "mock_stim_freq",
            "mock_stim_triangle",
            "mock_stim_amplitude",
            "mock_stim_noise"
        ]

    def update_outputs(self, new_values):
        self.current_settings.update(new_values)

    def get_outputs(self):
        return self.current_settings.copy()

    def acquisition_step(self, stage):
        if stage == 'wait':
            time.sleep(0.5)

        if stage == 'download':
            mock_signal_times = np.arange(self.current_settings['sample_count'], 
                                    dtype=np.float64) / \
                                self.current_settings['samplerate']
            mock_signal_periods = mock_signal_times * self.current_settings['mock_stim_freq']

            sample_channel_factor = 1e-8

            wrapped_buffers = []

            for _ in range(self.current_settings['channel_count']):
                mock_signal = (self.current_settings['mock_stim_amplitude'] 
                    * np.sin(2 * np.pi * mock_signal_periods))
                mock_signal += (self.current_settings['mock_stim_noise'] 
                    * np.random.standard_normal(self.current_settings['sample_count']))
                mock_signal += (self.current_settings['mock_stim_triangle'] 
                    * (np.mod(mock_signal_periods, 1) - 0.25))

                mock_signal /= sample_channel_factor
                mock_signal = (np.rint(mock_signal)).astype(int)

                wrapped_buffers.append(
                    BufferWrapper(mock_signal, 1.0 
                        / self.current_settings['samplerate'], sample_channel_factor))

            return {
                'channels': wrapped_buffers
            }

        return None


class UQDSDevice(Device):
    """uQDS Specific device implementation

    Details:
        This specialisation connects the Testbench functionality
        to a uQDS Crate connected via a standard FTDI USB

        It provides interfaces to set buffer size, channel settings, 
        and to trip the buffer acquisiton and record and process buffer data.
    """

    def __init__(self, uqds, register_map_path, config_block):
        super().__init__()

        self.uqds = uqds
        self.register_map = uqds.get_register_map(register_map_path, config_block)

        self.current_settings = {
            'sample_count': 32768,
            'channel_count': 16,
            'calibration_disabled': False,
            'decimation_enabled': False,
            'decimation': 7,
            'pga_setting': 1
        }

        self.available_outputs = [
            "channel_count",
            "sample_count",
            "calibration_disabled",
            'decimation_enabled',
            'decimation',
            'pga_setting'
        ]

    def get_samplerate(self):
        """Returns the samplerate in Hz
        """
        return 615384.0

    def get_buffer_length(self):
        """Returns the buffer length in samples
        """

        return self.current_settings['sample_count']

    def reset(self):
        self._reset_channel_gains()

        #self.uqds.write_register(0x00, [0x20, 0x00, 0x00, 0x11])
        #time.sleep(1)

        #self.uqds.reset()

        self.current_settings = {
            'sample_count': 32768,
            'channel_count': 16,
            'calibration_disabled': False,
            'decimation_enabled': False,
            'decimation': 8,
            'pga_setting': 1
        }

    def get_pga_config_setting(self):
        """Return the PGA hardware register code
        """

        pga_lut = {
            1: 0x00,
            9: 0x01,
            45: 0x02,
            450: 0x03
        }

        selected_pga_gain = self.current_settings['pga_setting']
        if not selected_pga_gain in pga_lut:
            raise ValueError(f"Selected PGA Gain of {selected_pga_gain} not available!")

        return pga_lut[selected_pga_gain]

    def _reset_channel_gains(self):
        channel_config = [0b100, 0x00, 0x00, 0x00]

        channel_settings_start = 19
        for channel_num in range(16):
            self.uqds.write_register(channel_num + channel_settings_start, channel_config)



    def _activate_channels(self):
        channel_config = [self.get_pga_config_setting() | 0b100, 0x00, 0x00, 0x00]

        # Set up the per-channel configurations
        #
        # Channel generic register map settings can be checked in the Register_Map.xls file!
        ch_settings = [
            # Channel generic A set to:
            # - Moving Average filter length of 0
            # - DAQ MUX Control to Gain/Offset output
            # - Decimation control value of n=1 for 2^n
            # - No inverter, no decimation, no median filer,
            #   no moving average filter, gain/offset correction ENABLED,
            #   no auto-offset
            [0x00, 0x00, 0x15, 0x40],
            # - Default channel gain correction value, no median filter
            [0x40, 0x00, 0x00, 0x00],
            # - Default offset correction value
            [0x00, 0x00, 0x00, 0x00],

            # - Unknown function, default 0
            [0x00, 0x00, 0x00, 0x00]
        ]

        if self.current_settings['decimation_enabled']:
            ch_settings[0][3] |= 1 << 1  # Enable decimation
            ch_settings[0][2] &= 0b1111
            ch_settings[0][2] |= self.current_settings['decimation'] << 4

        calibration_gain_register = self.register_map["GainCorrection" 
            + str(self.get_pga_config_setting())].number
        calibration_offset_register = self.register_map["OffsetCorrection" 
            + str(self.get_pga_config_setting())].number

        channel_settings_start = 19
        for channel_num in range(self.current_settings['channel_count']):
            self.uqds.write_register(channel_num + channel_settings_start, channel_config)

            self.uqds.write_register(channel_num * 4 + 128, ch_settings[0])

            if self.current_settings['calibration_disabled']:
                self.uqds.write_register(channel_num * 4 + 129, ch_settings[1])
                self.uqds.write_register(channel_num * 4 + 130, ch_settings[2])
            else:
                # Load channel flash bank 0 config (holds calibration data)
                self.uqds.write_register(0, [0x30, 0, channel_num, 0x02])
                time.sleep(0.1)
                channel_gain = self.uqds.read_reg(calibration_gain_register)
                channel_offset = self.uqds.read_reg(calibration_offset_register)

                if ((channel_gain == bytearray([0, 0, 0, 0])) 
                        or (channel_gain == bytearray([0xFF, 0xFF, 0xFF, 0xFF]))):
                    print(f'Channel {channel_num} does not seem to have proper gain calibration,'
                            ' using default...')
                    channel_gain = ch_settings[1]
                if ((channel_offset == bytearray([0, 0, 0, 0])) 
                        or (channel_offset == bytearray([0xFF, 0xFF, 0xFF, 0xFF]))):
                    print(
                f'Channel {channel_num} does not seem to have proper offset calibration,'
                            ' using default...')
                    channel_offset = ch_settings[2]

                self.uqds.write_register(channel_num * 4 + 129, channel_gain)
                self.uqds.write_register(channel_num * 4 + 130, channel_offset)

            self.uqds.write_register(channel_num * 4 + 131, ch_settings[3])

    def activate(self):
        uqds = self.uqds
        reg_map = self.register_map

        uqds.reset()
        time.sleep(0.5)

        # Disable all channel trigger sources. We only want to trigger on
        # our own trigger signal
        uqds.write_register(reg_map['pm_buffer_mask'].number, [0, 0, 0, 0])

        # Set up our readout config to the appropriate number of channels, raw
        # buffer readout, etc.
        uqds.write_register(reg_map['readout_config'].number,
                            [95 + self.current_settings['channel_count'], 0x00, 0x00, 0x00])

        uqds.write_register(reg_map['GlobalHeartBeatReg'].number, [0, 0, 0, 0x41])

        channel_per_ram = uqds.read_reg(7)
        channel_per_ram = channel_per_ram[2] & 0b1111

        # Set up the length of the buffer to have the right amount of samples,
        # clipping it to the minimum sample count of 2048
        sample_count = (self.current_settings['sample_count']*2*channel_per_ram / 4096) - 1
        sample_count = max(sample_count, 0)

        buffer_len_bytes = int(np.rint(sample_count))
        buffer_len_bytes &= 0xFF
        to_write = buffer_len_bytes << 8 | buffer_len_bytes
        to_write = int.to_bytes(to_write, 4, 'big')
        uqds.write_register(reg_map['buffer_config'].number, to_write)

        self._activate_channels()

        # Save to flash
        # The time delay afterwards is *vital*, as the uQDS may otherwise
        # only erase the flash but not save new values. This leads to
        # an all-0xFF looking flash
        uqds.write_register(0x00, [0x20, 0x00, 0x00, 0x11])
        time.sleep(1)

    def program_channel_id(self, channel_num, channel_id):
        """Progam a given channel ID to a numbered channel slot
        """

        channel_no_register_id = self.register_map['channel_no'].number
        channel_type_register_id = self.register_map['channel_type'].number

        channel_id_bytes = int(0).to_bytes(4, 'big')
        if channel_id > 0:
            channel_id_bytes = channel_id.to_bytes(4, 'big')

        self.uqds.write_register(0, [0x30, 0, channel_num, 0x02])
        time.sleep(0.1)
        channel_id_0 = self.uqds.read_reg(channel_no_register_id)

        self.uqds.write_register(0, [0x30, 0, channel_num, 0x12])
        time.sleep(0.1)
        channel_id_1 = self.uqds.read_reg(channel_no_register_id)

        if channel_id < 0:
            if channel_id_0 == (0xFFFFFFFF).to_bytes(4, 'big'):
                print(f'CH{channel_num}: Channel has no ID to load. Please specify a valid ID!')
                sys.exit()
            elif channel_id_0 != channel_id_1:
                print(f'CH{channel_num}:  Internal ID Missmatch! Channel data might be corrupt.')
                print(f'CH{channel_num}:  Bank 0 says {channel_id_0}, bank 1 says {channel_id_1}')
                print(f'CH{channel_num}: Cannot load ID. Please specify a valid ID to program!')
                sys.exit()

            loaded_id = int.from_bytes(channel_id_0, 'big')
            print(f'CH{channel_num}: Loaded channel id {loaded_id}')

            return loaded_id

        if channel_id_0 != channel_id_1:
            print(f'CH{channel_num}:  Internal ID Missmatch! Channel data might be corrupt.')
            print(f'CH{channel_num}:  Bank 0 says {channel_id_0}, bank 1 says {channel_id_1}')
            if input('Continue? [y/N]:').lower() != 'y':
                print('Aborting...')
                sys.exit()

        if channel_id_0 == (0xFFFFFFFF).to_bytes(4, 'big'):
            print(f'CH{channel_num}: Channel has no ID, programming it to {channel_id}!')
        elif channel_id_bytes != channel_id_0:
            print(f'CH{channel_num}: The Channel ID you specified does not line up with flash ID' +
                  f'CH{channel_num}: (Flash says {channel_id_0}, you say {channel_id_bytes})')
            if input('Overwrite? [y/N]:').lower() != 'y':
                print('Aborting...')
                sys.exit()
        else:
            print(f'CH{channel_num}: Channel ID was already programmed to the correct ID!')
            return channel_id

        # We have flash bank 1 loaded, we should write the new ID and type to it first
        self.uqds.write_register(channel_no_register_id, channel_id_bytes)
        self.uqds.write_register(channel_type_register_id, [0, 0, 0, 2])
        time.sleep(0.1)
        # Save these settings
        self.uqds.write_register(0, [0x30, 0, channel_num, 0x11])
        time.sleep(0.5)
        # Load flash bank 0, we want to save the ID there too
        self.uqds.write_register(0, [0x30, 0, channel_num, 0x02])
        time.sleep(0.1)
        # Load in new type and ID...
        self.uqds.write_register(channel_no_register_id, channel_id_bytes)
        self.uqds.write_register(channel_type_register_id, [0, 0, 0, 2])
        time.sleep(0.1)
        # And save it to flash bank 0
        self.uqds.write_register(0, [0x30, 0, channel_num, 0x01])
        time.sleep(0.5)

        return channel_id

    def setup_channel_calibrations(self, gains, offsets):
        """Write the array of channel calibration gains and offsets to channel flash
        """

        pga_setting = self.get_pga_config_setting()

        gains = np.array(gains)
        offsets = np.array(offsets)

        print(f'Saving channel calibration data: Gains are {gains}, offsets are {offsets}!')

        for channel_num in range(self.current_settings['channel_count']):
            channel_factor = self.register_map[f'ChannelData{channel_num}'].multiplier

            channel_gain = 1 / gains[channel_num]

            if (channel_gain > (1.3)) or (channel_gain < (0.7)):
                print(f'Channel {channel_num} gain was outside of acceptable range!'
                    ' Using default gain of 1...')
                channel_gain = int((1 << 22))
            else:
                channel_gain = int(np.rint(channel_gain * (1 << 22)))

            channel_gain = int.to_bytes(channel_gain << 8, 4, 'big', signed=False)

            channel_offset = -offsets[channel_num]
            channel_offset /= channel_factor
            channel_offset = int(np.rint(channel_offset))
            channel_offset_clipped = min((1 << 31) - 2, max(channel_offset, -(1 << 31) + 2))
            if channel_offset_clipped != channel_offset:
                print(
                    f'Channel {channel_num} offset value was clipped to {channel_offset_clipped}!'
                    '(Error of {100 - channel_offset_clipped / (channel_offset / 100)}%)')

            channel_offset = int.to_bytes(channel_offset_clipped, 4, 'big', signed=True)

            self.uqds.write_register(129 + 4 * channel_num, channel_gain)
            self.uqds.write_register(130 + 4 * channel_num, channel_offset)

            # Read channel config into Register Map Area
            self.uqds.write_register(0, [0x30, 0, channel_num, 0x02])
            time.sleep(0.2)

            # Write Gain and Offset correction to RAM locations
            self.uqds.write_register(self.register_map["GainCorrection" + str(pga_setting)].number,
                                     channel_gain)
            self.uqds.write_register(
                self.register_map["OffsetCorrection" + str(pga_setting)].number,
                                     channel_offset)
            time.sleep(0.1)
            # Write to channel Flash block 0
            self.uqds.write_register(0, [0x30, 0, channel_num, 0x01])
            time.sleep(0.5)

        # Save to flash
        # The time delay afterward is *vital*, as the uQDS may otherwise
        # only erase the flash but not save new values. This leads to
        # an all-0xFF looking flash
        self.uqds.write_register(0x00, [0x20, 0x00, 0x00, 0x11])
        time.sleep(1)

    def _download_data(self):
        uqds = self.uqds

        raw_buffers = uqds.read_buffer(self.current_settings['sample_count'],
                                       95,
                                       95 + self.current_settings['channel_count'])

        raw_buffers = np.transpose(raw_buffers)[1:]

        timestep = 1 / 615384.0

        wrapped_buffers = []

        for i in range(self.current_settings['channel_count']):
            channel_factor = self.register_map[f'ChannelData{i}'].multiplier

            buffer_line = raw_buffers[i]

            buffer_wrapper = BufferWrapper(buffer_line, timestep, channel_factor)
            wrapped_buffers.append(buffer_wrapper)

        return {
            'channels': wrapped_buffers
        }

    def acquisition_step(self, stage):
        uqds = self.uqds

        if stage == 'prepare':
            uqds.reset()
            time.sleep(0.5)

        if stage == 'trigger':
            uqds.write_register(0, [0x13, 0, 0, 0])

        if stage == 'wait':
            buffer_wait_start_time = time.time()
            time.sleep(0.1)
            while True:
                time.sleep(0.1)

                system_status = uqds.read_reg(uqds.system_status_reg)

                if int(system_status[3]) == 0x00:
                    break
                if (time.time() - buffer_wait_start_time) > 30:
                    print("Timeout while waiting for uQDS Buffer to be ready!")
                    raise RuntimeError("Timeout while waiting for uQDS Buffer to be ready!")

        if stage == 'download':
            # Reading the PM buffer to have the channel measurements.
            return self._download_data()

        return None

    def update_outputs(self, new_values):
        self.current_settings.update(new_values)


class UqdsSweepResultChannel:
    """Wrapper class to contain sweep results for a single channel

    Details:
        This class is used to reformat the data from a "one list of sweep data 
        for all channels combined" format into a "One list per channel" format,
        which is much more useful to deal with when parsing and analysing data.
    """

    def __init__(self, result_handler, slot_no):

        self.result_handler = result_handler
        self.slot_number = slot_no

        # Array of channel buffers
        self.buffers = []

        for _, row in self.result_handler.dataframe.iterrows():
            self.buffers.append(
                row['measurements'][self.result_handler.uqds_data_key]['channels'][slot_no])

        self.result_data = None

        self.result_failed = False

    def extract_measurement(self, key):
        """Convenience function to extract a uQDS Measurement across the sweep

        Details:
            Will return a 1D array of the given data key along the sweep
        """

        out_array = []

        for buffer in self.buffers:
            out_array.append(buffer.measurements[key])

        return out_array

    def get_nth_harmonics(self, n):
        """Extract a limited set of harmonics from the channel data.
        """

        harmonics = []

        for buffer in self.buffers:
            local_harmonic = buffer.measurements['harmonics_db']
            if np.size(local_harmonic) < n:
                local_harmonic = np.pad(local_harmonic, (0, n - np.size(local_harmonic)),
                            'constant',
                            constant_values=buffer.measurements['noise_floor_db'])

            harmonics.append(local_harmonic[0:n])

        return harmonics

    def extract_stimulus(self, key):
        """Convenience function to extract a stimulus (other device such as sine) from the sweep
        """

        return self.result_handler.extract_stimulus(key)

    def find_3db_point(self):
        """Analysis function to calculate the -3dB gain point for a frequency sweep
        """

        frequency_data = self.extract_measurement('ext_stimulus_frequency')
        attenuation_data = self.extract_measurement('stimulus_attenuation_db')

        prev_index = min(len(attenuation_data) - 2, np.searchsorted(attenuation_data, 3) - 1)

        prev_attenuation = attenuation_data[prev_index]
        next_attenuation = attenuation_data[prev_index + 1]

        prev_frequency = frequency_data[prev_index]
        next_frequency = frequency_data[prev_index + 1]

        crossing_percentage = (3 - prev_attenuation) / \
                              (next_attenuation - prev_attenuation)

        db_point = prev_frequency + crossing_percentage * (next_frequency - prev_frequency)

        if self.result_data is None:
            self.result_data = {}

        if np.isfinite(db_point):
            self.result_data['-3dB point'] = db_point

        return db_point

    def gather_stats_on(self, key):
        """Analytics function to gather generic statistics on a given key.
        
        Details:
            Will return a dict with various statistical parameters for the given key.
            These are: Max, Min, Average and Standard Deviation
        """
        data = self.extract_measurement(key)

        if self.result_data is None:
            self.result_data = {}

        self.result_data[key] = {
            'max': np.max(data),
            'min': np.min(data),
            'avg': np.average(data),
            'stddev': np.std(data)
        }

    def calculate_linearity(self, reference_data, slope_max_deviation=0.01, offset_max=0.001):
        """Convenience function to calculate linearity of the channels.

        Details:
            This function takes in a given set of reference voltages, and
            produces the offset and slope of this channel's measurements relative to it.
        """
        own_measurements = self.extract_measurement('average')

        a = np.vstack([reference_data, np.ones(len(reference_data))]).T
        slope, y_cut = numpy.linalg.lstsq(a, own_measurements, rcond=None)[0]

        if self.result_data is None:
            self.result_data = {}

        if np.isfinite(slope) and np.isfinite(y_cut):
            self.result_data['linearity'] = {
                'a': slope,
                'b': y_cut
            }

        if abs(slope - 1) > slope_max_deviation:
            self.result_failed = True
        if abs(y_cut) > offset_max:
            self.result_failed = True

    # pylint: disable=too-many-branches
    def evaluate_stats(self, eval_dict):
        """Convenience function to evaluate various statistics
        """

        if self.result_data is None:
            self.result_data = {}

        for key in eval_dict:
            if not key in self.result_data:
                self.gather_stats_on(key)

            eval_data = eval_dict[key]
            if not isinstance(eval_data, dict):
                continue

            results = self.result_data[key]

            for eval_type in eval_data:
                if eval_type == 'min':
                    if results['min'] < eval_data['min']:
                        self.result_failed = True
                if eval_type == 'max':
                    if results['max'] > eval_data['max']:
                        self.result_failed = True
                if eval_type == 'avg_min':
                    if results['avg'] < eval_data['avg_min']:
                        self.result_failed = True
                if eval_type == 'avg_max':
                    if results['avg'] > eval_data['avg_max']:
                        self.result_failed = True
                if eval_type == 'stddev_min':
                    if results['stddev'] < eval_data['stddev_min']:
                        self.result_failed = True
                if eval_type == 'stddev_max':
                    if results['stddev'] > eval_data['stddev_max']:
                        self.result_failed = True

        if self.result_failed:
            self.result_data['failed'] = True

    def get_fft_pcolormesh_data(self, stimulus_key, 
        start_frequency=None, end_frequency=None, num=256):
        """Helper function to get reduced FFT data for a Matplotlib Pcolormesh
        """

        out_meshdata = []
        out_frequencies = []

        stimulus_data = self.extract_stimulus(stimulus_key)

        for buffer in self.buffers:
            buffer_freqs, buffer_peaks = buffer.get_downsampled_fft_db(
                start_frequency=start_frequency,
                end_frequency=end_frequency, num=num,
                frequencies='average')

            out_meshdata.append(buffer_peaks)
            out_frequencies.append(buffer_freqs)

        return np.repeat(np.array(stimulus_data)[:, np.newaxis], np.shape(out_meshdata)[1],
                         axis=1), out_frequencies, out_meshdata


class UqdsSweepResultHandler:
    """Convenience class to represent a set of UQDS Channels and their respective data
    """

    def __init__(self, dataframe, uqds_data_key='uqds'):
        self.dataframe = dataframe
        self.uqds_data_key = uqds_data_key

        self.channels = []

        self._setup_channels()

    def _setup_channels(self):
        # We figure out channels using the first row
        row = self.dataframe.loc[0]
        channel_count = len(row['measurements'][self.uqds_data_key]['channels'])

        for ch in range(channel_count):
            self.channels.append(UqdsSweepResultChannel(self, ch))

    def configure_stimulus_sine(self, frequency, amplitude):
        """Configure a sine-wave stimulus for all measurements
        """

        for _, row in self.dataframe.iterrows():
            for uqds_buffer in row['measurements'][self.uqds_data_key]['channels']:
                uqds_buffer.set_stimulus(row[frequency], row[amplitude])

    def extract_measurement(self, key):
        """Extract a given measurement key from the Test Devices
        """

        extraction_values = []

        extracted_key = re.search(r"^([^\.]+)\.(.*)$", key)

        if extracted_key is None:
            for row in self.dataframe['measurements']:
                extraction_values.append(row[key])
        else:
            for row in self.dataframe['measurements']:
                extraction_values.append(row[extracted_key.group(1)][extracted_key.group(2)])

        return extraction_values

    def extract_channel_measurement(self, name):
        """Extract a specific measurement from the uQDS Channel data sets
        """

        extraction_values = []

        for channel in self.channels:
            extraction_values.append(channel.extract_measurement(name))

        return extraction_values

    def extract_stimulus(self, name):
        """Convenience function to get the array of a given stimulus key
        """

        return self.dataframe[name]

    def calculate_3db_points(self):
        """Convenience function to calculate the -3dB points for all channels
        """
        for channel in self.channels:
            channel.find_3db_point()

    def calculate_linearity(self, reference_data_key):
        """Convenience function to calculate linearities for all channels
        """
        reference_data = self.extract_measurement(reference_data_key)

        for channel in self.channels:
            channel.calculate_linearity(reference_data)

    def calculate_stats_on(self, key):
        """Convenience functions to calculate statistics on all channels for given key
        """

        for channel in self.channels:
            channel.gather_stats_on(key)

    def evaluate_stats(self, eval_data):
        """Convenience function to perform batch statistics on all channels
        """
        
        for channel in self.channels:
            channel.evaluate_stats(eval_data)
