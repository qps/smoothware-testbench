
import numpy as np
import scipy.fftpack

class BufferWrapper:
    """Buffer analytics class

    Details:
        This class is meant as a container for a generic, single-channel,
        constant-sample-time stream of measurement data.
        It provides useful analytical functions for time- and
        frequency-domain analysis, and aims to provide a unified
        central location to define how to process and handle data.

    Note:
        To achieve best performance for the FFT measurements,
        this class will only work with buffers that have the lenght
        of a power of two. No attempts at padding etc. are made.

    Example:
        # In this example, buffer data is taken to be sampled at
        # 1 MHz and is integer data at 1mV per LSB
        buffer_data = sensor.get_data()

        buffer = BufferWrapper(buffer_data, 1e-6, 0.001)

        # The buffer object can now be used to perform calculations
        # on the data

        # We tell the wrapper that we expected a stimulus signal of
        # 1kHz at 1V amplitude
        buffer.set_stimulus(1000, 1)

        thd = buffer.measurements['thd_db']

        # We can also get FFT peak interpolations
        # and find peaks at specific frequencies:
        peak_freq, peak_amplitude = buffer.get_fft_peak_db(1000)
    """

    def __init__(self, buffer, timestep, buffer_factor = 1):
        if len(np.shape(buffer)) != 1:
            raise ValueError(
                f'Buffer has wrong number of dimensions (shape is: {np.shape(buffer)})')
        if np.size(buffer) & (np.size(buffer) - 1):
            raise ValueError(f'Buffer length must be a power of two (is: {np.size(buffer)})')

        ## The unscaled integer buffer, a direct copy of the buffer parameter of the
        # constructor
        self.integer_buffer = buffer
        ## The data buffer scaled by buffer_factor to a sensible physical unit.
        self.raw_buffer = self.integer_buffer.astype('float64') * buffer_factor

        ## The scaling factor between raw data reported by the sensor, and a sensible
        # physical unit (V/LSB)
        self.buffer_factor = buffer_factor

        ## The timestep between measurement points, in seconds
        self.timestep = timestep

        ## The size of a FFT bin frequency step
        self.freq_step = 1 / (np.size(buffer) * timestep)
        ## The maximum frequency of our FFT (nyquist frequency)
        self.freq_stop = 1 / (2*timestep)

        ## Cache variable.
        # This variable stores a windowed version of raw_buffer, most
        # likely windowed with a hamming window, to reduce
        # FFT artefacts and noise.
        self.windowed_buffer = None

        ## Cache variable of the output of a FFT calculation
        # Only magnitude, starting at 0Hz and going up in freq_step steps
        # to freq_stop
        self.fft = None
        ## Cache variable, FFT magnitude in dB
        self.fft_db = None
        
        self.get_fft_db()

        ## Measurements storage dict
        # This dict contains various measurements related to this buffer.
        # It holds most of the outputs of the analysis functions of this class.
        #
        # The following values (non-exhaustive list) *may* be stored
        # - Always generated:
        #   - Statical values: average, min, max, stddev
        #   - noise_floor_db: The value of the 80% percentile of the FFT, in db.
        #          Used as reasonable flat noise floor estimate
        # - Generated by set_stimulus:
        #   - ext_stimulus_frequency/amplitude: The provided frequency and amplitude
        #       passed to this function
        #   - stimulus_frequency, stimulus_amplitude: The detected frequency and amplitude
        #       as given by the get_fft_peak() function, which uses FFT peak interpolation
        #       to deliver accurate results even for off-bin-center frequencies
        #   - stimulus_attenuation(_db): The attenuation (reference/measured), as multiplicative
        #       factor and in dB. Note that a failure to calculate attenuation results in a 200dB
        #       default (NaN/Inf not supported by PostgreSQL JSONB)
        #   - thd(_db): The total harmonic distortion of the signal. Calculated in _calculate_thd(),
        #       in essence the energy of the main frequency peak divided by the sum of the
        #       harmonics (calculated up to the 20th)
        #     - This will also store a list of the harmonic amplitudes in dB in the measurements
        #       attribute, under 'harmonics_db'
        # - Generated by calculate_histogram:
        #   - histogram: A dict of two lists ('buckets' and 'counts')
        #      representing the histogram of the integer_buffer data

        self.measurements = {
            'average'   : np.average(self.raw_buffer[10:-10]),
            'min'       : np.min(self.raw_buffer[10:-10]),
            'max'       : np.max(self.raw_buffer[10:-10]),
            'stddev'    : np.std(self.raw_buffer[10:-10]),
            'noise_floor_db': np.percentile(self.get_fft_db(), 80)
        }


    def _calculate_fft(self):
        raw_fft = scipy.fftpack.fft(self.get_windowed_signal())
        self.fft = np.abs(raw_fft[0:(np.size(raw_fft) // 2)])

        # Output here in sine amplitude, not Peak To Peak
        fft_scale_factor = self.get_window_scaling_factor() * 2
        fft_scale_factor /= np.size(raw_fft)

        self.fft *= fft_scale_factor
        return self.fft

    def _calculate_thd(self, frequency):
        root_frequency, root_amplitude = self.get_fft_peak(frequency)
        root_amplitude_db = -200

        if root_amplitude > 0:
            root_amplitude_db = 20*np.log10(root_amplitude)

        n_harmonic = 2
        harmonic_amplitude_sum = 0

        harmonic_amplitudes_db = [root_amplitude_db]

        while (root_frequency * n_harmonic) < self.freq_stop:
            if n_harmonic > 20:
                break

            search_freq = root_frequency * n_harmonic
            _, harmonic_amplitude = self.get_fft_peak(search_freq)
            harmonic_amplitude_db = 20*np.log10(harmonic_amplitude)
            
            harmonic_amplitudes_db.append(harmonic_amplitude_db)
            
            harmonic_amplitude_sum += harmonic_amplitude ** 2
            n_harmonic += 1

        harmonic_amplitude_sum **= 0.5

        self.measurements['harmonics_db'] = harmonic_amplitudes_db

        return harmonic_amplitude_sum / root_amplitude

    def _calculate_snr(self, frequency):
        """
        Warning:
            This function may not produce sensible SNR outputs just yet.
            It still needs to be fixed up!

        Todo:
            Figure out what exactly to do for the SNR calculation
        """

        if frequency >= self.freq_stop:
            raise ValueError(f"Frequency {frequency} is above Nyquist threshold!")
        if frequency < 0:
            raise ValueError(f"Frequency {frequency} is below zero!")
        
        local_fft = self.get_fft().copy()
        signal_frequency, _ = self.get_fft_peak(frequency)

        frequency_bin = int(signal_frequency / self.freq_step)

        local_fft = np.delete(local_fft, np.arange(0, 5))
        local_fft = np.delete(local_fft, np.arange(frequency_bin-2 - 5, frequency_bin+3 -5))

        noise_power = np.average(local_fft ** 2)

        signal_power = np.average(self.get_fft()[frequency_bin-2:frequency_bin+3])

        if np.isfinite(signal_power):
            self.measurements['signal_power'] = signal_power
        if np.isfinite(noise_power):
            self.measurements['noise_power'] = noise_power

        if np.isfinite(signal_power) and noise_power != 0:
            snr = signal_power / noise_power
            if np.isfinite(snr):
                self.measurements['snr'] = snr
                self.measurements['snr_db'] = 20 * np.log10(self.measurements['snr'])

    def calculate_histogram(self):
        """Calculate a histogram on the raw data.

        Details:
            This function will run a histogram calculation
            on the raw, integer data.

            This histogram will be saved in the `measurements` attribute
            under the key 'histogram', in the form of a dict containing
            two lists:
                {
                    'buckets': histogram_buckets,
                    'counts':  samples_in_this_bucket
                }
            This is to facilitate better database storage of histogram data,
            as JSON serialisation only supports strings as object keys.

        Returns:
            None
        """

        hist_data = {}

        for raw_value in self.integer_buffer:
            if raw_value not in hist_data:
                hist_data[raw_value]  = 1
            else:
                hist_data[raw_value] += 1

        buckets = []
        counts = []

        for bucket in hist_data:
            buckets.append(int(bucket))
            counts.append(int(hist_data[bucket]))

        self.measurements['histogram'] = {
            'buckets': buckets,
            'counts':  counts
        }

    def set_stimulus(self, stimulus_frequency, stimulus_amplitude):
        """
        Set the stimulus frequency and amplitude and calculate parameters.

        Details:
            This function will tell the buffer that the measured data should contain a
            reference signal with given frequency and amplitude.
            This reference data is then used to perform a series of useful statistical
            calculations on the data.

            The following parameters will be added to the `measurements` attribute:

            - ext_stimulus_frequency/amplitude: The provided frequency and amplitude
                passed to this function
            - stimulus_frequency, stimulus_amplitude: The detected frequency and amplitude
                as given by the get_fft_peak() function, which uses FFT peak interpolation
                to deliver accurate results even for off-bin-center frequencies
            - stimulus_attenuation(_db): The attenuation (reference/measured), as multiplicative
                factor and in dB. Note that a failure to calculate attenuation results in a 200dB
                default (NaN/Inf not supported by PostgreSQL JSONB)
            - thd(_db): The total harmonic distortion of the signal. Calculated in _calculate_thd(),
                in essence the energy of the main frequency peak divided by the sum of the
                harmonics (calculated up to the 20th)
                - This will also store a list of the harmonic amplitudes in dB in the measurements
                attribute, under 'harmonics_db'

        Args:
            stimulus_frequency: The frequency (in Hz) of the stimulus signal
            stimulus_amplitude: The amplitude (in the unit of the calibrated/scaled output of the
                sensor) that is expected to be measured

        Note:
            Calling this function multiple times will overwrite previous values.
            It is therefore currently only possible to handle a single sine-wave
            stimulus signal as input!

        Returns:
            None
        """

        self.measurements['ext_stimulus_frequency'] = stimulus_frequency
        self.measurements['ext_stimulus_amplitude'] = stimulus_amplitude

        if (stimulus_frequency is None) or (stimulus_amplitude is None):
            raise ValueError("Both stimulus frequency and amplitude must be given!")
        if (stimulus_frequency < self.freq_step) or (stimulus_frequency >= self.freq_stop):
            raise ValueError(
                f"Stimulus frequency is outside of usable frequency resolution!"
                f"(Minimum {self.freq_step}, max {self.freq_stop}, is {stimulus_frequency})")

        measured_freq, measured_ampl = self.get_fft_peak(stimulus_frequency)

        self.measurements['stimulus_frequency'] = measured_freq
        self.measurements['stimulus_amplitude'] = measured_ampl

        attenuation = stimulus_amplitude / measured_ampl
        self.measurements['stimulus_attenuation'] = attenuation
        if attenuation > 0:
            attenuation_db = 20 * np.log10(self.measurements['stimulus_attenuation'])
            self.measurements['stimulus_attenuation_db'] = attenuation_db
        else:
            self.measurements['stimulus_attenuation_db'] = 200

        thd = self._calculate_thd(self.measurements['ext_stimulus_frequency'])
        self.measurements['thd'] = thd
        if thd > 0:
            self.measurements['thd_db'] = 20 * np.log10(self.measurements['thd'])
        else:
            self.measurements['thd_db'] = -200

        self._calculate_snr(self.measurements['ext_stimulus_frequency'])

    def get_windowed_signal(self):
        """Return a windowed copy of the raw_buffer.

        Details:
            This function will return a copy of the buffer, with a windowing
            function (default hanning) applied.

        Note:
            The windowed data is *not* scaled back out.
            Multiply it with get_window_scaling_factor() to get
            accurate averages or FFT values!

        Returns:
            numpy.array
        """

        if self.windowed_buffer is not None:
            return self.windowed_buffer

        window_frame = np.hanning(np.size(self.raw_buffer))
        self.windowed_buffer = self.raw_buffer * window_frame

        return self.windowed_buffer

    def get_window_scaling_factor(self):
        """Returns the factor to multiply to a windowed signal from get_windowed_signal().

        @see get_windowed_signal()
        """

        return 2

    def get_fft(self):
        """Returns a numpy array of FFT magnitudes

        Details:
            This function will calculate the FFT and return a numpy
            array of all frequency bins from 0Hz to freq_stop.

        Returns:
            numpy.array of frequency bins

        @see get_fft_db()
        """

        if self.fft is not None:
            return self.fft

        self.fft = self._calculate_fft()
        return self.fft

    def get_fft_db(self):
        """Returns a numpy array of FFT magitudes in dB

        Details:
            This function will calculate the FFT and return a numpy
            array of all frequency bin magnitudes, in dB, from 0Hz to freq_stop.

        Returns:
            numpy.array of frequency bin magitudes, in dB

        @see get_fft()
        """

        if self.fft_db is not None:
            return self.fft_db

        fft = self.get_fft()
        fft = np.clip(fft, 1e-10, np.inf)

        self.fft_db = 20 * np.log10(fft)

        return self.fft_db

    def get_fft_peak_db_between(self, start_frequency, stop_frequency):
        """Calculate the interpolated frequency peak in a given frequency range.

        Details:
            This function will attempt to search for the highest peak in a
            given frequency range. It does so by first seeking for the
            frequency bin with the highest magnitude, and then performing
            a FFT interpolation for enhanced result precision even then the
            FFT peak may not lie exactly in a FFT bound.

            The function has been guarded against interpolation failures, and
            should always return a reasonable result.
            Please note that it always returns a result, even when no distinct
            peak could be found.

            The FFT interpolation maths have been taken from:
            https://ccrma.stanford.edu/~jos/sasp/Quadratic_Interpolation_Spectral_Peaks.html

        Note:
            This is mostly an internal function. get_fft_peak_db() is
            more convenient to use.

        @see get_fft_peak_db, get_fft_peak

        Returns:
            peak_frequency, peak_magnitude (in dB)
        """

        if start_frequency >= stop_frequency:
            raise ValueError("Stop frequency has to be larger than start frequency!")

        if stop_frequency > self.freq_stop:
            raise ValueError(f"Frequency {stop_frequency} is above Nyquist threshold!")
        if start_frequency < 0:
            raise ValueError(f"Frequency {start_frequency} is below zero!")

        frequency_min = start_frequency
        frequency_max = stop_frequency

        fft_db = self.get_fft_db()
        fft_len = np.size(fft_db)

        frequency_min = max(1, min(fft_len-1, int(round(frequency_min / self.freq_step))))
        frequency_max = max(frequency_min+1,
                            min(fft_len, int(round(frequency_max / self.freq_step))))

        frequency_range = fft_db[frequency_min:frequency_max]
        peak_bin_index = np.argmax(frequency_range) + frequency_min

        try:
            # FFT Interpolation taken from:
            # https://ccrma.stanford.edu/~jos/sasp/Quadratic_Interpolation_Spectral_Peaks.html

            prev_bin = fft_db[max(0, peak_bin_index-1)]
            peak_bin = fft_db[peak_bin_index]
            next_bin = fft_db[min(fft_len-1, peak_bin_index+1)]

            # This can happen if our search returns a result right at the
            # edge of the search bucket, and the next value IS higher.
            # Our parabolic interpolation can then shoot off way into the sky.
            if (next_bin > peak_bin) or (prev_bin > peak_bin):
                return peak_bin_index*self.freq_step, fft_db[peak_bin_index]

            interpolated_peak_bin = 0

            # Catch a random "all are equal" peak. Can happen, can throw the parabola off!
            if (prev_bin - 2*peak_bin + next_bin) != 0:
                interpolated_peak_bin = 1/2.0 * ((prev_bin - next_bin) /
                    (prev_bin - 2*peak_bin + next_bin))

            # If this happens, our interpolation went awry.
            # This value *should* stay between +-0.5 of our peak bin
            if (interpolated_peak_bin < -0.5) or (interpolated_peak_bin > 0.5):
                return peak_bin_index*self.freq_step, fft_db[peak_bin_index]

            interpolated_peak_magnitude  = peak_bin
            interpolated_peak_magnitude -= 1/4.0 * (prev_bin-next_bin)*interpolated_peak_bin
            interpolated_peak_frequency  = (peak_bin_index + interpolated_peak_bin) * self.freq_step

            if interpolated_peak_frequency < 0:
                return peak_bin_index*self.freq_step, fft_db[peak_bin_index]

            return interpolated_peak_frequency, interpolated_peak_magnitude
        # pylint: disable=bare-except
        except:
            return peak_bin_index*self.freq_step, fft_db[peak_bin_index]

    def get_fft_peak_db(self, frequency, search_width = 0.03):
        """Calculate the interpolated frequency peak around a search frequency.

        Details:
            This function will attempt to search for the highest peak
            in a certain range around a given frequency (default of +-3% deviation).
            It does so by first seeking for the
            frequency bin with the highest magnitude, and then performing
            a FFT interpolation for enhanced result precision even then the
            FFT peak may not lie exactly in a FFT bound.

            The function has been guarded against interpolation failures, and
            should always return a reasonable result.
            Please note that it always returns a result, even when no distinct
            peak could be found.

        Returns:
            peak_frequency, peak_magnitude (in dB)

        @see get_fft_peak_db_between, get_fft_peak
        """

        frequency_start = max(0, frequency * (1-search_width))
        frequency_stop  = min(self.freq_stop, frequency * (1+search_width))
        return self.get_fft_peak_db_between(frequency_start, frequency_stop)

    def get_fft_peak(self, frequency, search_width = 0.03):
        """Calculate the interpolated frequency peak around a search frequency.

        Details:
            This function will attempt to search for the highest peak
            in a certain range around a given frequency (default of +-3% deviation).
            It does so by first seeking for the
            frequency bin with the highest magnitude, and then performing
            a FFT interpolation for enhanced result precision even then the
            FFT peak may not lie exactly in a FFT bound.

            The function has been guarded against interpolation failures, and
            should always return a reasonable result.
            Please note that it always returns a result, even when no distinct
            peak could be found.

        @see get_fft_peak_db_between, get_fft_peak_db

        Returns:
            peak_frequency, peak_magnitude
        """
        ipeak_frequency, ipeak_magnitude = self.get_fft_peak_db(frequency, search_width)

        return ipeak_frequency, 10 ** (ipeak_magnitude / 20)
    
    #! pylint: disable=too-many-locals
    def get_downsampled_fft_db(self, start_frequency = None, end_frequency = None,
                num = 1024, frequencies='exact'):
        """Return a log-downsampled array of FFT magnitudes

        Details:
            This function will downsample the full FFT into maximum `num` points, with
            logarithmic frequency scaling (hence the lower range not containing as many FFT bins
            and potentially not every downsampled point containing a FFT bin).

            Each downsampling point is made of a pair of frequency and maximum/peak
            FFT bin magnitude.

        Warning:
            This function has not been fully tested and may not be accurate.
            Inspecting FFT values is best done via the get_fft_peak functions.

        Returns:
            downsampled_frequencies, downsampled_peaks
        """
        if start_frequency is None:
            start_frequency = self.freq_step
        if end_frequency is None:
            end_frequency   = self.freq_stop

        start_bin = int(np.floor(start_frequency/self.freq_step))
        stop_bin  = np.floor(end_frequency/self.freq_step)

        frequency_buckets = np.logspace(np.log10(start_bin+1), np.log10(stop_bin), num)
        frequency_bin_steps = np.floor(frequency_buckets).astype(int)

        output_peaks = []
        output_frequencies = []

        lower_bin_bound = start_bin

        for upper_bin_bound in frequency_bin_steps:
            if lower_bin_bound == upper_bin_bound:
                continue

            if upper_bin_bound == lower_bin_bound+1:
                output_frequencies.append(lower_bin_bound * self.freq_step)
                output_peaks.append(self.get_fft_db()[lower_bin_bound])
            else:
                lower_freq = lower_bin_bound*self.freq_step
                upper_freq = upper_bin_bound*self.freq_step
                peak_freq, peak_amplitude = self.get_fft_peak_db_between(lower_freq,
                                                                         upper_freq)
                
                if frequencies == 'exact':
                    output_frequencies.append(peak_freq)
                else:
                    log_bin_middle = (np.log10(lower_bin_bound)+np.log10(upper_bin_bound))/2
                    point_frequency = self.freq_step * (10 ** log_bin_middle)
                    output_frequencies.append(point_frequency)

                output_peaks.append(peak_amplitude)

            lower_bin_bound = upper_bin_bound
        
        return output_frequencies, output_peaks
