# Smoothware Testbench

This repository holds the Smoothware Testbench python code package. This code is used to perform hardware testing and verification, mainly for the uQDS Channel testing system.

It has two main purposes:
- To provide an easy and abstracted interface to hardware devices and test bench setups, in a reusable and repeatable fashion.
    - Testbench device access is abstracted behind simple key/value pairs, testbenches can be set up and reset in repeatable fashion
- To provide an interface to configure test executions of hardware
    - Test executions can be configured to run measurements with varying parameters, such as linearly varying a DC voltage source, or logarithmically going through a frequency range.

It also has a second folder (bin), which is configured specifically for the uQDS Testbench. It can serve as an example for how to set up the connection to devices
and test setups.

## Doxygen source docs

There are automatic [Doxygen source docs available here](http://smoothware-testbench.docs.cern.ch/), and as such, this README won't go into detail on such.

## Looking for an owner!

This project was made by David Bailey during his temporary internship. Although it is functional, it would be nice to have someone there to maintain and understand it.

All I ask is that you treat it carefully, and use proper pull-requests to merge code, honouring the CI pipelines. We do clean code here.

## Table of contents
1. [Config File Reference](#config-file-quick-reference) to describe the config file options
2. [Installation](#installation)
3. [Database Setup](#database-setup)
4. [Grafana Setup](#grafana-setup)

## Installation

The code itself has not fully been configured as python package, but can be added to the include path of your python env. 
It only needs two prerequisites:

- Numpy
- Pandas

Should be fairly simple. *However*, device drivers, uQDS connections or similar *are not handled by this* and must be added separately.
This library contains wrappers for common systems such as DC sources, function generators, switching matrices etc., and it's encouraged to hook
into these wrappers or write your own following their style.

### uQDS Testbench

The uQDS testbench has a few extra needs, as it requires a lot of drivers for the actual measurement hardware.
Sadly, these systems were not necessarily properly cleaned up, and the setup relies on a rather specific PyCharm instance that has multiple include paths.

The basics are, however:

1. Instantiating all the device drivers as needed
2. Instantiating the SmoothwareTB Testbench and configuring device drivers there.
3. Configuring a database as according to the setup section
4. Configuring the actual test setups with the Test Sweep system
5. Running them

Check out the `bin` folder of this directory. It contains all the test setups for the uQDS channel testbench.

## Database Setup

This code uses a simple PostgreSQL database as backend. You will need to set it up a little before being able to use this script.

### Initial setup

For users at CERN, this can be done by following the [DBOD setup guide for PostgreSQL](https://dbod-user-guide.web.cern.ch/getting_started/PostgreSQL/postgresql/).

You will also need to tweak the `pg_hba.conf` file to allow connection of your users. In the DBOD panel, File Editor again, `pg_hba.conf`.
Make sure your own database user can get access, e.g.:
`host   all     admin   0.0.0.0/0   md5`

Then, to set up a minimal database:
- Connect to the DB with your admin credentials.
- Create a new user+database for the testbench
- If needed, create a Grafana read-only user to plot data with.

```SQL
CREATE USER [username] PASSWORD [password];
CREATE DATABASE [db_name] WITH OWNER [username];

\c [db_name]

CREATE USER [grafana-username] PASSWORD [grafana-password];
GRANT USAGE ON SCHEMA public TO [grafana-username];
GRANT SELECT ON ALL TABLES IN SCHEMA public TO [grafana-username];

ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO [grafana-username];
```

You now need to go back to the `pg_hba.conf` file to add the testbench and Grafana users. Add the lines:
```
host  [db_name]  [japc_user]    0.0.0.0/0   md5
host  [db_name]  [grafana-user] 0.0.0.0/0   md5 
```

Check if you can connect to them via `psql` now, after the database was reloaded.

Once you configured the testbench, it will create the necessary tables for you - no further table setup is needed for this!
Also, check the Doxygen documentation, which [describes the Table Schema](https://smoothware-testbench.docs.cern.ch/classsmoothwaretb_1_1database_1_1testbench__db__handler_1_1TestbenchDBHandler.html#a8034b17210ce4e031ea93c3fe3f28954) a bit more.



## Grafana Setup

[Grafana](https://grafana.com/) is the recommended observability platform to use alongside this adapter to plot and monitor data. CERN already has some instances appropriately set up, the recommended one being [the MONIT instance](https://monit-grafana.cern.ch/). Your section should be able to request access to it and receive its own organization within it.

You can easily add the DBOD PostgreSQL instance as standard Postgres instance data source in Grafana. Use the username+password combination supplied during [the database setup](#database-setup) - using the readonly Grafana user, NOT the Testbench-user!

From here, it is recommended to use the [getting started guides](https://grafana.com/docs/grafana/latest/getting-started/build-first-dashboard/) to plot your data.
There are backups for the uQDS Channel grafana panels available in the `grafana-panels` folder.

Also, for plots such as heatmaps or non-timeseries data, the [Plotly.JS Grafana panel](https://github.com/nline/nline-plotlyjs-panel) works great, if you know Plotly and some very simple Javascript. Much better for high-point-count plots (>2k and above), as well as heatmap plots or similar.

## Support

Originally this script was created by David Bailey, who is most likely no longer at CERN (unless you get lucky and I'm back ;) )

His E-Mail is davidbailey.2889@gmail.com

Otherwise, Magnus Christensen had a hand in setting up some Grafana items as well.

## Authors and acknowledgment

Primary Author: David Bailey (davidbailey.2889@gmail.com)

With thanks to: Magnus Christensen for giving Grafana a footing here,
    Jens Steckert for letting me work on this next to his precious uQDS Channels.
    Some personal thanks also go to my friends Neira and Mesh, for providing experience with database setups and flexible schema layout.

## License
MIT, baby. Take it, use it, enjoy it, make it.