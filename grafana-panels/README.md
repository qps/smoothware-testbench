# Grafana panel backups

These JSON files are backups of the Grafana panels used in the CERN uQDS Channel Testbench.
They can be imported into a Grafana instance such as MonIT, and should be connected to the same PostgreSQL database that the Testbench itself is writing to!

